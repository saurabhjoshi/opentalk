package test;

import java.io.IOException;
import java.util.Map;

import org.json.JSONException;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.callLogPojo.CallLogResponse;
import com.google.gson.Gson;
import com.missCallLogPojo.MissCallLogResponse;

import retrofit2.Call;
import retrofit2.Response;

public class TalkRequestTests extends TestBase {
	
	public TalkRequestTests () throws IOException
	{
		super.properties = AppProperties.getInstance(null);
	}
	
	
	@Test(priority = 0)
	public void isMyTalkCircle()
	{
		Reporter.log("Verifying that the user is on my talk circle.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().clicksOnMyTalkCircle().isUserOnMyTalkCircle());
	}
	
	@Test(priority = 1)
	public void verifyingUserInfoInMyTalkCircle()
	{
		Reporter.log("Verifying user info in my talk circle." , true);
		Map<String , String> m =  OpenTalkUsers.getInstance().getPrimaryUser().getUserInfoOnMyTalkCircle();
		String userName = m.get("userName");
	    Reporter.log("username visible in mytalk circle - > " + userName, true);
	    Assert.assertTrue(userName.contains(properties.getPropertyValue("secondUserName")));
	    
	    String lan = m.get("openTalks");
	    Reporter.log("opentalks  - > " + lan, true);
	    Assert.assertTrue(lan.contains("Opentalks"));
	    
	    String topic = m.get("location");
	    Reporter.log("location  - > " + topic, true);

	    String text = m.get("text");
	    Reporter.log("text  - > " + text, true);
	    Assert.assertTrue(text.contains("Request To Talk"));
		
	}
	
	@Test(priority = 2)
	public void isCreatsTalkRequestScreen()
	{
		Reporter.log("Verifying that by clicking on the + icon the user is redirected to the Request to Talk screen.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().clicksOnCreateTalkRequestIcon().isRequestToTalkScreen());
	}
	
	@Test(priority = 3)
	public void isMyTalkCircleAfterCreatingTalkRequest()
	{
		Reporter.log("Verifying that the user is on my talk circle after creating a talk request.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().createTalkRequest().isUserOnMyTalkCircle());
	}
	
	@Test(priority = 4)
	public void verifyingUserInfoInMyTalkCircleAfterRequest()
	{
		Reporter.log("Verifying user info in my talk circle after creating talk request." , true);
		Map<String , String> m =  OpenTalkUsers.getInstance().getPrimaryUser().getUserInfoOnMyTalkCircle();
		String userName = m.get("userName");
	    Reporter.log("username visible in mytalk circle - > " + userName, true);
	    Assert.assertTrue(userName.contains(properties.getPropertyValue("secondUserName")));
	    
	    String lan = m.get("openTalks");
	    Reporter.log("opentalks  - > " + lan, true);
	    Assert.assertTrue(lan.contains("Opentalks"));
	    
	    String topic = m.get("location");
	    Reporter.log("location  - > " + topic, true);

	    String text = m.get("text");
	    Reporter.log("text  - > " + text, true);
	    Assert.assertTrue(text.contains("Edit Request"));
		
	}
	
	@Test(priority = 5)
	public void isSentReceivedScreen() throws InterruptedException
	{
		Reporter.log("Verifying that the user is on sent received screen after creating a talk request icon.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().clicksOnSentReceivedIcon().isSentReceivedRequestScreen());
	}
	
	@Test(priority = 6)
	public void verifyingTalkRequestInfoInSentRequests()
	{
		Reporter.log("Verifying talk request info in sent requests." , true);
		Map<String , String> m =  OpenTalkUsers.getInstance().getPrimaryUser().clicksOnSentTab().getTalkRequestInfoOnSent();
		String userName = m.get("userName");
	    Reporter.log("username visible in sent request - > " + userName, true);
	    Assert.assertTrue(userName.contains(properties.getPropertyValue("secondUserName")));
	    
	    String lan = m.get("circleName");
	    Reporter.log("circleName is  - > " + lan, true);
	    Assert.assertTrue(lan.contains("My Talk Circle"));
	    
		
	}
	
	@Test(priority = 7)
	public void verifyingTalkRequestInfoInReceivedRequestsForSecondUser() throws InterruptedException
	{
		Reporter.log("Verifying talk request info in recevied requests for the second user." , true);
		Map<String , String> m =  OpenTalkUsers.getInstance().getSecondryUser().clicksOnSentReceivedIcon().getTalkRequestInfoOnReceived();
		String userName = m.get("userName");
	    Reporter.log("username visible in recevied requests - > " + userName, true);
	    Assert.assertTrue(userName.contains(properties.getPropertyValue("firstUserName")));
	    
	    String lan = m.get("circleName");
	    Reporter.log("circleName is in recevied request  - > " + lan, true);
	    Assert.assertTrue(lan.contains("My Talk Circle"));
	    
	    String Location = m.get("location");
	    Reporter.log("requester location is  - > " + Location, true);
	  //  Assert.assertTrue(Location.contains("Wants to Reconnect")); 
	    
		
	}
	
	@Test(priority = 8)
	public void isFindingPartnerScreen()
	{
		Reporter.log("Verifying that the user is on finding partner.", true);
		OpenTalkUsers.getInstance().getPrimaryUser().doBackground();
		OpenTalkUsers.getInstance().getSecondryUser().clicksOnCallIcon();
		
	}
	
	@Test(priority = 9)
	public void isCallIdInMissCallLogDB() throws InterruptedException, IOException, JSONException{
	
		Reporter.log("Getting Call Id - "+ OpenTalkUsers.getInstance().getSecondryUser().gettingCallId(), true);
		Thread.sleep(55000);
		
		SoftAssert soft =new SoftAssert();
		Reporter.log("Verifying that the call id is there in miss call log DB.", true);
		Reporter.log("Verify the response of miss call API.", true);
		RetrofitService service = ServiceGenerator.createService(RetrofitService.class, properties.getPropertyValue("baseUrl"));
		Reporter.log("Base URL is " + properties.getPropertyValue("baseUrl"), true);
		
	    Call<MissCallLogResponse> call = service.getMissCallLog(OpenTalkUsers.getInstance().getSecondryUser().callId);
	    Response<MissCallLogResponse> response = call.execute();
	    Reporter.log("respone - " + new Gson().toJson(response.body().getData()),true);
		Reporter.log("Verifying the response code.", true);
		soft.assertTrue(response.body().getCode().equals(200));
		Reporter.log("response code is " + response.body().getCode() , true);
		
		Reporter.log("Verifying the reason status.", true);
		String reasonStatus = response.body().getData().getCallDetails().getReasonStatus().toString();
		Reporter.log("reason Status is - " + reasonStatus , true);
		soft.assertTrue(reasonStatus.contains("4"));
		
		Reporter.log("Verifying the call type.", true);
		soft.assertTrue(response.body().getData().getCallDetails().getCallType().contains("talk_request"));
		Reporter.log("Call type is " + response.body().getData().getCallDetails().getCallType(), true);
	
	    soft.assertAll();
	}
	
	@Test(priority = 9)
	public void verifyUserInfoOnConnectingScreen()
	{
	    Reporter.log("The user clicking on the call icon.", true);
	    OpenTalkUsers.getInstance().getSecondryUser().clicksOnCallIcon();
		Reporter.log("Verifying the connecting user info." , true);
		Map<String , String> m =  OpenTalkUsers.getInstance().getSecondryUser().getUserInfoOnConnectingScreen();
		String userName = m.get("userName");
	    Reporter.log("Connecting username - > " + userName, true);
	    Assert.assertTrue(userName.contains(properties.getPropertyValue("firstUserName")));
	    
	    String lan = m.get("languages");
	    Reporter.log("Connecting user language  - > " + lan, true);
	    Assert.assertTrue(lan.contains(properties.getPropertyValue("firstUserLanguage")));
	    
	    String topic = m.get("topic");
	    Reporter.log("Connecting user topic  - > " + topic, true);
	    Assert.assertTrue(topic.contains("Reconnect Request"));
	    
	}
	
	@Test(priority = 10)
	public void isConnectedScreen()
	{
		Reporter.log("Verifying that the call connected screen is visible after the call connecting screen when call connects." , true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().isConnected());
		Reporter.log("First User is connected.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().isConnected());
		Reporter.log("Both users are connected." , true);
		
	}
	
	@Test(priority = 11)
	public void verifyInfoOnConnectedScreen()
	{
		Reporter.log("Verifying the connected user info." , true);
		Map<String , String> m =  OpenTalkUsers.getInstance().getPrimaryUser().getInfoOnConnectedScreen();
		String userName = m.get("userName");
	    Reporter.log("Connected username - > " + userName, true);
	    Assert.assertTrue(userName.contains(properties.getPropertyValue("secondUserName")));
	    
	    
	    String status = m.get("status");
	    Reporter.log("status  - > " + status, true);
	    Assert.assertTrue(status.contains("Connected"));
	    
	}
	
	@Test(priority = 12)
	public void ExtendCallEnabled()
	{
		Reporter.log("Verifying that extend call button is enabled.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().isExtendCallEnabled());
	}
	
	@Test(priority = 13)
	public void ExtendForFirstTime() throws InterruptedException
	{
		Reporter.log("Verifying that call extends when auser clicks on the extend call button .", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().isCallExtended());
	}
	
	@Test(priority = 14)
	public void isCallExtendForSecondTime() throws InterruptedException
	{
		Reporter.log("Verifying that call extends when auser clicks on the extend call button .", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().isCallExtended());
	}
	
	@Test(priority = 15)
	public void UserOnRateCall() throws InterruptedException, JSONException{
		
		Reporter.log("Getting Call Id - "+ OpenTalkUsers.getInstance().getPrimaryUser().gettingCallId(), true);
		Reporter.log("Verifying that the user on rate call screen after clicks  on stop connecting.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().clicksOnEndCall().isRateCallScreen());
		Reporter.log("Verifying that second user is also on Rate screen.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().isRateCallScreen());
	}
	
	@Test(priority = 16)
	public void verifyInfoOnRateScreen()
	{
		Reporter.log("Verifying the connected user info on rate call screen." , true);
		Map<String , String> m =  OpenTalkUsers.getInstance().getPrimaryUser().getInfoOnRateScreen();
		String userName = m.get("userName");
	    Reporter.log("Connected username on rate call screen - > " + userName, true);
	    Assert.assertTrue(userName.contains(properties.getPropertyValue("secondUserName")));
	    
		Reporter.log("Verifying the second connected user info on rate call screen." , true);
		Map<String , String> m1 =  OpenTalkUsers.getInstance().getSecondryUser().getInfoOnRateScreen();
		String userName1 = m1.get("userName");
	    Reporter.log("Connected username on rate call screen - > " + userName1, true);
	    Assert.assertTrue(userName1.contains(properties.getPropertyValue("firstUserName")));
	    
	}
	
	@Test(priority = 17)
	public void isComplementScreen(){
		
		Reporter.log("Verifying that the user on complement screen after clicks  on thumbs up.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().cliksOnThumbsUp().isComplementScreen());
		Reporter.log("Verifying that the second user on complement screen after clicks  on thumbs up.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().cliksOnThumbsUp().isComplementScreen());
	}
	
	@Test(priority = 18)
	public void isSentReceviedScreenAfterThumbsupForFirstUser() throws InterruptedException{
		
		Reporter.log("Verifying that the user on sent recevied screen after submit.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().clicksOnSubmitOnComplement().isSentReceivedRequestScreen());

	}
	
	
	@Test(priority = 19)
	public void isSentReceviedScreenAfterThumbsupForSecondUser() throws InterruptedException{
		
		Reporter.log("Verifying that the user on sent recevied screen after submit..", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().clicksOnSubmitOnComplement().isSentReceivedRequestScreen());
		
	}
	@Test(priority = 20)
	public void isCallIdInCallLogDB() throws InterruptedException, IOException, JSONException{
		SoftAssert soft =new SoftAssert();
		Reporter.log("Verifying that the call id is there in call log DB.", true);
		Reporter.log("Verify the response of Call Connected API.", true);
		RetrofitService service = ServiceGenerator.createService(RetrofitService.class, properties.getPropertyValue("baseUrl"));
		Reporter.log("Base URL is " + properties.getPropertyValue("baseUrl"), true);
		
	    Call<CallLogResponse> call = service.getCallLog(OpenTalkUsers.getInstance().getPrimaryUser().callId);
	    Response<CallLogResponse> response = call.execute();
	    Reporter.log("respone - " + new Gson().toJson(response.body().getData()),true);
		Reporter.log("Verifying the response code.", true);
		soft.assertTrue(response.body().getCode().equals(200));
		Reporter.log("response code is " + response.body().getCode() , true);
		
		Reporter.log("Verifying the disconnect by.", true);
		String disconnectBy = response.body().getData().getCallDetails().getDisconnectBy().toString();
		Reporter.log("disconnect by - " + disconnectBy , true);
		soft.assertTrue(disconnectBy.contains(OpenTalkUsers.getInstance().getPrimaryUser().gettingAppLogs().get("userId")));
		Reporter.log("Disconnect by user id is ." +OpenTalkUsers.getInstance().getPrimaryUser().gettingAppLogs().get("userId") , true);
       
		
		Reporter.log("Verifying the call type.", true);
		soft.assertTrue(response.body().getData().getCallDetails().getCallType().contains("talk_request"));
		Reporter.log("Call type is " + response.body().getData().getCallDetails().getCallType(), true);
	
	    soft.assertAll();
	}
	

}
