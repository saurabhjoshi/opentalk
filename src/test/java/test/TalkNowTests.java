package test;

import java.io.IOException;
import java.util.Map;

import org.json.JSONException;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.callLogPojo.CallLogResponse;
import com.google.gson.Gson;
import com.removeUserPojo.RemoveUserResponse;

import retrofit2.Call;
import retrofit2.Response;

public class TalkNowTests extends TestBase {
	
	
	public TalkNowTests() throws IOException{
		super.properties = AppProperties.getInstance(null);
	}

	
	@Test(priority = 0)
	public void talkNowDisabled()
	{
		Reporter.log("Verifying that default talk now button is disabled for both the users." , true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().isTalkNowDisabled());
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().isTalkNowDisabled());
		Reporter.log("Buttons are disabled." , true);
		
	}
	
	@Test (priority = 1)
	public void talkNowEnabled()
	{
		Reporter.log("Verifying that after selecting any topic the talk now button is enabled." , true);
		OpenTalkUsers.getInstance().getPrimaryUser().selectTopic(Constant.firstUserTopicIndex);
	//	Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().isTalkNowEnabled());
		OpenTalkUsers.getInstance().getSecondryUser().selectTopic(Constant.secondUserTopicIndex);
	//	Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().isTalkNowEnabled());
		Reporter.log("Buttons are enabled." , true);
	}
	
	@Test (priority = 2)
	public void isFindingPartnerScreen()
	{
		Reporter.log("Verifying that the user is redirected to the finding partner screen after clicking on the talk now button." , true);
		OpenTalkUsers.getInstance().getPrimaryUser().TalkNow();
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().isFindingPartnerScreen());
		OpenTalkUsers.getInstance().getSecondryUser().TalkNow();
//		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().isFindingPartnerScreen());
		Reporter.log("Both users are on finding partner screen." , true);
	}
	
	@Test(priority = 3)
	public void isConnectingScreen()
	{
		Reporter.log("Verifying that the connecting screen is visible after the finding partner screen when call connects." , true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().isConnecting());
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().isConnecting());
		Reporter.log("Both users are connecting." , true);
		
	}
	
	@Test(priority = 4)
	public void verifyUserInfoOnConnectingScreen()
	{

		Reporter.log("Verifying the connecting user info." , true);
		Map<String , String> m =  OpenTalkUsers.getInstance().getPrimaryUser().getUserInfoOnConnectingScreen();
		String userName = m.get("userName");
	    Reporter.log("Connecting username - > " + userName, true);
	    Reporter.log("Name from properties - "+super.properties.getPropertyValue("secondUserName") , true);
	    Assert.assertTrue(userName.equalsIgnoreCase(super.properties.getPropertyValue("secondUserName")));
	    
	    String lan = m.get("languages");
	    Reporter.log("Connecting user language  - > " + lan, true);
	    Assert.assertTrue(lan.contains(super.properties.getPropertyValue("secondUserLanguage")));
	    
	    String topic = m.get("topic");
	    Reporter.log("Connecting user topic  - > " + topic, true);
	    Assert.assertTrue(topic.contains(OpenTalkUsers.getInstance().getPrimaryUser().topic));

	}
	
	@Test(priority = 5)
	public void isConnectedScreen()
	{
		Reporter.log("Verifying that the call connected screen is visible after the call connecting screen when call connects." , true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().isConnected());
		Reporter.log("First User is connected.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().isConnected());
		Reporter.log("Both users are connected." , true);
		
	}
	
	@Test(priority = 6)
	public void verifyInfoOnConnectedScreen()
	{
		
		Reporter.log("Verifying the connected user info." , true);
		Map<String , String> m =  OpenTalkUsers.getInstance().getPrimaryUser().getInfoOnConnectedScreen();
		String userName = m.get("userName");
	    Reporter.log("Connected username - > " + userName, true);
	    Assert.assertTrue(userName.contains(super.properties.getPropertyValue("secondUserName")));
	    
 
	    String status = m.get("status");
	    Reporter.log("status  - > " + status, true);
	    Assert.assertTrue(status.contains("Connected"));
	    
	}
	

	@Test(priority = 7)
	public void isCallExtendForFirstTime() throws InterruptedException
	{
		Reporter.log("Verifying that call extends when auser clicks on the extend call button .", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().isCallExtended());
	}
	

	
	@Test(priority = 8)
	public void isCallExtendForSecondTime() throws InterruptedException
	{
		Reporter.log("Verifying that call extends when auser clicks on the extend call button .", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().isCallExtended());
	}
	
	@Test(priority = 9)
	public void UserOnRateCall() throws InterruptedException, JSONException{
		OpenTalkUsers.getInstance().getPrimaryUser().gettingAppLogs();
		Reporter.log("Getting Call Id - "+ OpenTalkUsers.getInstance().getPrimaryUser().gettingCallId(), true);
		Reporter.log("Verifying that the user on rate call screen after clicks  on stop connecting.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().clicksOnEndCall().isRateCallScreen());
		Reporter.log("Verifying that second user is also on Rate screen.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().isRateCallScreen());
	}
	
	@Test(priority = 10)
	public void verifyInfoOnRateScreen()
	{
		
		Reporter.log("Verifying the connected user info on rate call screen." , true);
		Map<String , String> m =  OpenTalkUsers.getInstance().getPrimaryUser().getInfoOnRateScreen();
		String userName = m.get("userName");
	    Reporter.log("Connected username on rate call screen - > " + userName, true);
	    Assert.assertTrue(userName.contains(super.properties.getPropertyValue("secondUserName")));
	    
		Reporter.log("Verifying the second connected user info on rate call screen." , true);
		Map<String , String> m1 =  OpenTalkUsers.getInstance().getSecondryUser().getInfoOnRateScreen();
		String userName1 = m1.get("userName");
	    Reporter.log("Connected username on rate call screen - > " + userName1, true);
	    Assert.assertTrue(userName1.contains(properties.getPropertyValue("firstUserName")));
	    
	}
	
	
	
	
	@Test(priority = 11)
	public void isComplementScreen(){
		
		Reporter.log("Verifying that the user on complement screen after clicks  on thumbs up.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().cliksOnThumbsUp().isComplementScreen());
		Reporter.log("Verifying that the second user on complement screen after clicks  on thumbs up.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().cliksOnThumbsUp().isComplementScreen());
	}
	
	@Test(priority = 12)
	public void isFindingPartnerScreenAfterThumbsupForFirstUser() throws InterruptedException{
		
		Reporter.log("Verifying that the user on finding partner screen.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().clicksOnSubmitOnComplement().isFindingPartnerScreen());

	}
	
	@Test(priority = 13)
	public void isHomeAfterStopeConnectingForFirstUser() throws InterruptedException{
		
		Reporter.log("Verifying that the user on Home screen.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().clicksOnStopConnecting().isHomeScreen());

	}
	
	@Test(priority = 14)
	public void isFindingPartnerScreenAfterThumbsupForSecondUser() throws InterruptedException{
		
		Reporter.log("Verifying that the second user on finding partner screen.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().clicksOnSubmitOnComplement().isFindingPartnerScreen());
	}
	
	@Test(priority = 15)
	public void isHomeAfterStopeConnectingForSecondUser() throws InterruptedException{
		
		Reporter.log("Verifying that the user on Home screen.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().clicksOnStopConnecting().isHomeScreen());

	}
	
	@Test(priority = 16)
	public void isCallIdInCallLogDB() throws InterruptedException, IOException, JSONException{
		SoftAssert soft =new SoftAssert();
		Reporter.log("Verifying that the call id is there in call log DB.", true);
		Reporter.log("Verify the response of Call Connected API.", true);
		RetrofitService service = ServiceGenerator.createService(RetrofitService.class, properties.getPropertyValue("baseUrl"));
		Reporter.log("Base URL is " + properties.getPropertyValue("baseUrl"), true);
		
	    Call<CallLogResponse> call = service.getCallLog(OpenTalkUsers.getInstance().getPrimaryUser().callId);
	    Response<CallLogResponse> response = call.execute();
	    Reporter.log("respone - " + new Gson().toJson(response.body().getData()),true);
		Reporter.log("Verifying the response code.", true);
		soft.assertTrue(response.body().getCode().equals(200));
		Reporter.log("response code is " + response.body().getCode() , true);
		
		Reporter.log("Verifying the disconnect by.", true);
		String disconnectBy = response.body().getData().getCallDetails().getDisconnectBy().toString();
		Reporter.log("disconnect by - " + disconnectBy , true);
		soft.assertTrue(disconnectBy.contains(OpenTalkUsers.getInstance().getPrimaryUser().gettingAppLogs().get("userId")));
		Reporter.log("Disconnect by user id is ." +OpenTalkUsers.getInstance().getPrimaryUser().gettingAppLogs().get("userId") , true);
       
		
		Reporter.log("Verifying the call type.", true);
		soft.assertTrue(response.body().getData().getCallDetails().getCallType().contains("talk_now"));
		Reporter.log("Call type is " + response.body().getData().getCallDetails().getCallType(), true);
	
	    soft.assertAll();
	}
	
	
	
	

}

