package test;

import com.callLogPojo.CallLogResponse;
import com.google.gson.JsonElement;
import com.missCallLogPojo.MissCallLogResponse;
import com.removeUserPojo.RemoveUserResponse;
import com.userProfilePojo.UserProfileResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitService {
	
	
//	@GET("/orders/{order_id}")
	//Call<OrderResponse> postOrder(@Path("order_id")String order_id,@Body RequestBody body);
	
	@GET("qa/remove-user")
	Call<RemoveUserResponse> removeUser(@Query("user_id") String user);
	
	@GET("qa/missed-call-details")
	Call<MissCallLogResponse> getMissCallLog(@Query("call_id") String callId);
	
	@GET("qa/call-details")
	Call<CallLogResponse> getCallLog(@Query("call_id") String callId);
	
	@GET("qa/user-details")
	Call<UserProfileResponse> getUserProfile(@Query("user_id") String user);
	
}
