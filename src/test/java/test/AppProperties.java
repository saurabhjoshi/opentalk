package test;

import java.io.IOException;
import java.util.Properties;

public class AppProperties {
	
	private static AppProperties appProperties = null;
	
	private Properties properties;
	
	private AppProperties(){
	}
	
	public synchronized  static AppProperties getInstance(String env) throws IOException {
		if(appProperties==null){
			appProperties = new AppProperties();
		}
		if(env != null){
			appProperties.properties = new Properties();
			if(env.equalsIgnoreCase("prod")){
				appProperties.properties.load(AppProperties.class.getResourceAsStream("/prod.properties"));
			}else{
				appProperties.properties.load(AppProperties.class.getResourceAsStream("/stage.properties"));
			}
		}
		return appProperties;
	}
	
	public String getPropertyValue(String property){
		return this.properties.getProperty(property);
	}

}
