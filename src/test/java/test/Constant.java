package test;

public class Constant {
	
	public static final int firstUserTopicIndex = 0;
	public static final String firstUserTalkRequestNote = "Test Note";
	public static final String firstUserTalkStatus = "Test Status";
	public static final String firstUserServerIp = "127.0.0.1";
	public static final int firstUserServerPort = 8002;
	

	public static final int secondUserTopicIndex = 0;
	public static final String secondUserServerIp = "127.0.0.1";
	public static final int secondUserServerPort = 8003;

}
