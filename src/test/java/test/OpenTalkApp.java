package test;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import org.bouncycastle.asn1.cmp.ProtectedPart;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;

public class OpenTalkApp{
	
	WebDriver driver;
	WebDriverWait wait;
	
	public String topic;
	public String userId;
	public String callId;
	public String connectedTime;
	private AppProperties properties;
	
	public OpenTalkApp(String apk, String androidVersion, String serverIP ,int serverPort, String deviceName, String appPackage, String appActivity, String udid ) throws MalformedURLException{
		
		Reporter.log("Setting the Desired Capabilities.", true);
		
		String URL = "http://"+serverIP+":"+serverPort+"/wd/hub";
		Reporter.log("URl - " + URL , true);
		File classpathRoot = new File(System.getProperty("user.dir"));
		File appDir = new File(classpathRoot, "/Apps/");
		File app = new File(appDir, apk);
	
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(MobileCapabilityType.UDID, udid);
		capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
		capabilities.setCapability("deviceName", deviceName);
		capabilities.setCapability("platformVersion", androidVersion);
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("app", app.getAbsolutePath());
		capabilities.setCapability("appPackage", appPackage);
		capabilities.setCapability("appActivity", appActivity);
		capabilities.setCapability("idle-timeout", 500);
		capabilities.setCapability("autoAcceptAlerts", true);
		capabilities.setCapability("unicodeKeyboard", true);
		capabilities.setCapability("resetKeyboard", true);
		capabilities.setCapability("autoGrantPermissions", true);
		capabilities.setCapability("deviceOrientation", "portrait");
		capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 300);


		Reporter.log("Initializing User's AndroidDriver.", true);
		driver = new AppiumDriver(new URL(URL), capabilities);
		//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		wait = new WebDriverWait(driver, 15);
	}
	
	public void setAppProperties(AppProperties properties){
		this.properties = properties;
	}

	public OpenTalkApp doFBSignIn(String username, String password, String Lang) throws InterruptedException
	{
		Reporter.log("User clicking on the Login button.", true);
		wait.until(ExpectedConditions.elementToBeClickable(By.id(properties.getPropertyValue("appId")+":id/btn_fb_login")))
				.click();

		Reporter.log("Entering email.", true);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.className("android.widget.EditText")));
		Thread.sleep(3000);
		List<WebElement> e = driver.findElements(By.className("android.widget.EditText"));
		e.get(0).sendKeys(username);
		Reporter.log("Clicking on the Login button.", true);
		wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.Button"))).click();
		Thread.sleep(1000);
		List<WebElement> e1 = driver.findElements(By.className("android.widget.EditText"));
		if (e1.size() == 2) {
			Reporter.log("Entering password.", true);
			e1.get(1).sendKeys(password); // test password
		} else {
			Reporter.log("Entering password.", true);
			e1.get(0).sendKeys(password);
		}
		Reporter.log("Clicking on the Login button.", true);
		wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.Button"))).click();
		Thread.sleep(2000);
		Reporter.log("Clicking on the continue button.", true);
		wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.Button"))).click();

			if (isElementPresentOnboarding(By.id(properties.getPropertyValue("appId")+":id/txt_done")))
			{
				Reporter.log("Selecting Gender.", true);
				driver.findElement(By.id(properties.getPropertyValue("appId")+":id/gv_gender")).findElements(By.className("android.widget.TextView")).get(1).click();
				Reporter.log("User Clicking on Continue Button.", true);
				wait.until(ExpectedConditions.elementToBeClickable(By.id(properties.getPropertyValue("appId")+":id/txt_done"))).click();

				Reporter.log("user un selecting English language.", true);
				List<WebElement> rl_main = driver.findElement(By.id(properties.getPropertyValue("appId")+":id/rv_language")).findElements(By.id(properties.getPropertyValue("appId")+":id/rl_main"));
				rl_main.get(0).click();
				rl_main.get(1).click();
				rl_main.get(2).click();
				rl_main.get(3).click();

				Reporter.log("Clicking on the search icon of user.", true);
				wait.until(ExpectedConditions.elementToBeClickable(By.id(properties.getPropertyValue("appId")+":id/menu_search")))
						.click();

				Reporter.log("Entering language of the user.", true);
				AndroidElement search = (AndroidElement) wait
						.until(ExpectedConditions.visibilityOfElementLocated(By.id(properties.getPropertyValue("appId")+":id/search_src_text")));
				search.sendKeys(Lang);
				Reporter.log("Entering language -> " + Lang, true);
				Reporter.log("Selecting language -> " + Lang, true);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(properties.getPropertyValue("appId")+":id/txt_language")))
						.click();

				Reporter.log("Clicking on submit button of user.", true);
				List<WebElement> submit = driver.findElements(By.className("android.widget.ImageButton"));
				submit.get(1).click();

				Thread.sleep(2000);

				Reporter.log("Clicking on submit button of user.", true);
				submit.get(1).click();
				
				
			} else {
				Reporter.log("User is already Logged In.", true);
				
				
			}

			if (isElementPresent(MobileBy.xpath("//*[@class='android.widget.Button'][2]"))){
				Reporter.log("User clicking on the Allow button.", true);
				while (driver.findElements(MobileBy.xpath("//*[@class='android.widget.Button'][2]")).size()>0) 

				 {  driver.findElement(MobileBy.xpath("//*[@class='android.widget.Button'][2]")).click();
				 }
				}
			
			return this;
	}
	
	public boolean isLoggedIn()
	{
		return true;
	}
	
	public boolean verifyUsername(String username)
   {
		Reporter.log("Clicking on user's profile.", true);
		List <WebElement> userProfile = driver.findElements(By.id(properties.getPropertyValue("appId")+":id/bottom_navigation_small_container"));
		userProfile.get(2).click();

		Reporter.log("Factching the user name.", true);
		WebElement userName = driver.findElement(By.id(properties.getPropertyValue("appId")+":id/et_name"));
		Reporter.log("Verify the User Name.", true);
		if(userName != null)
		{
			 try {
				 Assert.assertTrue(userName.getText().contains(username));
				 return true;
			 }catch (Exception e)
			 {
				 Reporter.log(e.getMessage(), true);;
			 return false; 
			 }
		}
		else{
			Reporter.log("User Name is not visible or element is not there.", true);
			return false;
		}

		
	}

	
	public String selectTopic (int visibleIndex)
	{
		Reporter.log("Clicking on Home button.", true);
		List <WebElement> homeIcon10 = driver.findElements(By.id(properties.getPropertyValue("appId")+":id/bottom_navigation_small_container"));
		homeIcon10.get(0).click();
	    Reporter.log("Selecting Explore.", true);
		List <WebElement> tab = driver.findElements(By.className("android.support.v7.app.ActionBar$Tab"));
		tab.get(0).click();

		Reporter.log("Selecting topic for the user.", true);
		List<WebElement> childElements = driver.findElement(By.id(properties.getPropertyValue("appId")+":id/rv_topics"))
				.findElements(By.id(properties.getPropertyValue("appId")+":id/card_main"));
		WebElement Selected_Topic = childElements.get(visibleIndex);
		String topic = Selected_Topic.findElement(By.id(properties.getPropertyValue("appId")+":id/txt_topic_name"))
				.getText();
		Reporter.log("User topic -> " + topic, true);
		Selected_Topic.click();
		this.topic = topic;
		return topic;
		
	}
	public OpenTalkApp TalkNow(){
		Reporter.log("Clicking on the Talk Now button.", true);
		driver.findElement(By.id(properties.getPropertyValue("appId")+":id/card_talk_now")).click();
	//	wait.until(ExpectedConditions.elementToBeClickable(By.id(properties.getPropertyValue("appId")+":id/card_talk_now"))).click();
				
		return this;
	}
	
	public boolean isTalkNowDisabled()
	{
		try{
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(properties.getPropertyValue("appId")+":id/card_talk_now")));
		Assert.assertTrue(!driver.findElement(By.id(properties.getPropertyValue("appId")+":id/card_talk_now")).isEnabled());
	    return true;
		
		}catch(Exception e)
		{
			Reporter.log(e.getMessage(), true);;
			return false;
			}
		
	}
	
	public boolean isTalkNowEnabled()
	{
		try{
		Assert.assertTrue(driver.findElement(By.id(properties.getPropertyValue("appId")+":id/card_talk_now")).isEnabled());
	    return true;
		
		}catch(Exception e)
		{
			Reporter.log(e.getMessage(), true);;
			return false;
			}
		
	}
	
	public boolean isFindingPartnerScreen()
	{
		try{
	    Thread.sleep(500);
		Assert.assertTrue(isElementPresent(By.id(properties.getPropertyValue("appId")+":id/txt_finding_partner")));
		return true;
		
		}catch(Exception e)
		{
			Reporter.log(e.getMessage(), true);
			return false;
			}
		}
	
	public boolean isConnecting()
	{
		try{
		Assert.assertTrue(isElementPresent(By.id(properties.getPropertyValue("appId")+":id/txt_user_name")));
	    return true;
		
		}catch(Exception e)
		{
			Reporter.log(e.getMessage(), true);;
			return false;
			}
		}
	
	public Map<String, String> getUserInfoOnConnectingScreen(){
		Map<String, String> userInfo = new HashMap<>();
		Reporter.log("Getting connecting username.", true);
		String connecting_username = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.id(properties.getPropertyValue("appId")+":id/txt_user_name")))
				.getText();

		Reporter.log("Getting connecting user's languages.", true);
		String connecting_user_languages = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.id(properties.getPropertyValue("appId")+":id/txt_languages")))
				.getText();

		Reporter.log("Getting connecting user's topic.", true);
		String connecting_user_topic = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.id(properties.getPropertyValue("appId")+":id/txt_topic")))
				.getText();

		userInfo.put("userName", connecting_username);
		userInfo.put("languages", connecting_user_languages);
		userInfo.put("topic", connecting_user_topic);
		
		return userInfo;
	}
	
	
	public boolean isConnected(){
		try{
		Assert.assertTrue(isElementPresent(By.id(properties.getPropertyValue("appId")+":id/tv_entend_call")));
	    return true;
		
		}catch(Exception e)
		{
			Reporter.log(e.getMessage(), true);;
			return false;
			}
	}
	
	public boolean isExtendCallEnabled(){
		
		try{
		Assert.assertTrue(driver.findElement(By.id(properties.getPropertyValue("appId")+":id/tv_entend_call")).isEnabled());
	    return true;
		
		}catch(Exception e)
		{
			Reporter.log(e.getMessage(), true);;
			return false;
			}
		
	}
	
	public boolean isMicrophoneDisabled()
	{
		try{
		Assert.assertTrue(!driver.findElement(By.id(properties.getPropertyValue("appId")+":id/iv_mute")).isEnabled());
	    return true;
		
		}catch(Exception e)
		{
			Reporter.log(e.getMessage(), true);;
			return false;
			}
		
	}
	
	public int getRemainingTime(){
		
/*		String remainingTime = wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.id(properties.getPropertyValue("appId")+":id/text_call_duration")))
				.getText();*/
		String remainingTime = driver.findElement(By.id(properties.getPropertyValue("appId")+":id/text_call_duration")).getText();
		
		String sub = remainingTime.substring(0, 2);
		Reporter.log("Remaining Time in String -> " +sub , true);
		int remainingTimeInInteger = Integer.parseInt(sub);
		return remainingTimeInInteger;
		
	}
	
	public boolean isCallExtended() throws InterruptedException{
		
		Reporter.log("Getting remaining time. ", true);
		int remainingTimeInInteger = getRemainingTime();
		Reporter.log("Time before extend -> " + remainingTimeInInteger, true);
		Reporter.log("User clicking on the extend call button.", true);
/*		wait.until(ExpectedConditions.elementToBeClickable(By.id(properties.getPropertyValue("appId")+":id/tv_entend_call")))
				.click();*/
		driver.findElement(By.id(properties.getPropertyValue("appId")+":id/tv_entend_call"))
				.click();

		Thread.sleep(3000);
		Reporter.log("Getting remaining time. ", true);
		int remainingTimeAfterExtendInInteger = getRemainingTime();
		Reporter.log("Time after extend -> " + remainingTimeAfterExtendInInteger, true);
		if (remainingTimeAfterExtendInInteger >remainingTimeInInteger)
		{return true;}
		else {return false;}
		
		
	}
	
	public OpenTalkApp clicksOnEndCall() throws InterruptedException{
		
		Thread.sleep(3000);
	//	driver.manage().timeouts().implicitlyWait(3, TimeUnit.MINUTES);
		Reporter.log("Clicking on the stop connecting button.", true);
		wait.until(ExpectedConditions.elementToBeClickable(By.id(properties.getPropertyValue("appId")+":id/ib_end_call")))
				.click();
		return this;
	}
	public boolean isRateCallScreen(){

		
		try{
			Assert.assertTrue(isElementPresent(By.id(properties.getPropertyValue("appId")+":id/iv_thumbs_up")));
		    return true;
			
			}catch(Exception e)
			{
				Reporter.log(e.getMessage(), true);;
				return false;
				}
		
	}
	
	public boolean isHomeScreen() throws InterruptedException{

		if(isElementPresent(By.id(properties.getPropertyValue("appId")+":id/ib_close")))
		{
			Reporter.log("Clicking on the close reconnect popup icon.", true);
			driver.findElement(By.id(properties.getPropertyValue("appId")+":id/ib_close")).click();
		}
		
		try{
			Assert.assertTrue(isElementPresent(By.id(properties.getPropertyValue("appId")+":id/card_talk_now")));
		    return true;
			
			}catch(Exception e)
			{
				Reporter.log(e.getMessage(), true);;
				return false;
				}
		
	}
	
	public OpenTalkApp cliksOnThumbsUp(){
		
		Reporter.log("Clicking on the thumbs up button.", true);
		wait.until(ExpectedConditions.elementToBeClickable(By.id(properties.getPropertyValue("appId")+":id/iv_thumbs_up")))
				.click();
		
		return this;
	}
	
	public boolean isComplementScreen(){

		try{
			Assert.assertTrue(isElementPresent(By.id(properties.getPropertyValue("appId")+":id/card_submit")));
		    return true;
			
			}catch(Exception e)
			{
				Reporter.log(e.getMessage(), true);;
				return false;
				}

		
	}
	
	public OpenTalkApp clicksOnSubmitOnComplement() throws InterruptedException{
		
/*		Reporter.log("Clicking on the submit button.", true);
		wait.until(ExpectedConditions.elementToBeClickable(By.id(properties.getPropertyValue("appId")+":id/card_submit")))
				.click();*/
		
		while (isElementPresent(By.id(properties.getPropertyValue("appId")+":id/card_submit")))
				{
			Reporter.log("Clicking on the submit button.", true);
			driver.findElement(By.id(properties.getPropertyValue("appId")+":id/card_submit")).click();
				}
		
		return this;
		
	}
	
	public OpenTalkApp clicksOnStopConnecting(){
		
		Reporter.log("Clicking on stop connecting button.", true);
		wait.until(ExpectedConditions.elementToBeClickable(By.id(properties.getPropertyValue("appId")+":id/iv_stop_connecting")))
				.click();
		
		return this;
		
	}

	
	public Map<String, String> getInfoOnConnectedScreen(){
		Map<String, String> userInfo = new HashMap<>();
		Reporter.log("Getting connected username.", true);
		String connectedUsername = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.id(properties.getPropertyValue("appId")+":id/txt_user_name")))
				.getText();

/*		Reporter.log("Getting remaining time.", true);
		String remainingTime = wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.id(properties.getPropertyValue("appId")+":id/text_call_duration")))
				.getText();
		Reporter.log("Remaining time is - > " + remainingTime , true);*/


		Reporter.log("Getting calling status.", true);
		String status = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.id(properties.getPropertyValue("appId")+":id/text_calling_status")))
				.getText();

		userInfo.put("userName", connectedUsername);
//		userInfo.put("remainingTime", remainingTime);
		userInfo.put("status", status);
		
		return userInfo;
	}
	
	public Map<String, String> getInfoOnRateScreen(){
		Map<String, String> userInfo = new HashMap<>();
		Reporter.log("Getting connecting username.", true);
		String connectedUsername = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.id(properties.getPropertyValue("appId")+":id/txt_user_name")))
				.getText();

		Reporter.log("Getting remaining time.", true);
		String connectedTime = wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.id(properties.getPropertyValue("appId")+":id/text_call_duration")))
				.getText();
		Reporter.log("Remaining time is - > " + connectedTime , true);


		userInfo.put("userName", connectedUsername);
		userInfo.put("connectedTime", connectedTime);
		
		this.connectedTime = connectedTime;
		
		return userInfo;
	}
	
	public void quit(){
		driver.quit();
	}
	
	public  boolean isElementPresent(By by) throws InterruptedException  {
    
		try {
			WebDriverWait w = new WebDriverWait (driver,8);
			w.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
			return true;
		} catch (Exception e) {
			return false;
		}

	}
	
	public  boolean isElementPresentOnboarding(By by) throws InterruptedException  {
	    
		try {
			WebDriverWait w = new WebDriverWait (driver,15);
			w.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public Map<String, String> gettingAppLogs () throws JSONException
	{
		String text = driver.findElement(By.id("com.opentalk.staging:id/btn_automation")).getText();
		Reporter.log("App Logs - > " + text, true);
		JSONObject jsonObj= new JSONObject(text);
		Map<String, String> userInfo = new HashMap<>();
		if (jsonObj.has("User Id"))
		{
		Reporter.log("User Id - > " + jsonObj.getString("User Id"), true);
		userInfo.put("userId", jsonObj.getString("User Id"));
		}
		if (jsonObj.has("Auth Token"))
		{
		Reporter.log("Auth Token - > " + jsonObj.getString("Auth Token"), true);
		userInfo.put("auth", jsonObj.getString("Auth Token"));
		}
		if (jsonObj.has("Device ID"))
		{
		Reporter.log("Device ID - > " + jsonObj.getString("Device ID"), true);
		userInfo.put("deviceId", jsonObj.getString("Device ID"));
		}
		if (jsonObj.has("Version"))
		{
		Reporter.log("Version - > " + jsonObj.getString("Version"), true);
		userInfo.put("version", jsonObj.getString("Version"));
		}
	
		return userInfo;
		
	}
	
	public String gettingErrorLogs () throws JSONException
	{
		String text = driver.findElement(By.id("com.opentalk.staging:id/btn_automation")).getText();
		JSONObject jsonObj= new JSONObject(text);
		if (jsonObj.has("Toast"))
		{
		Reporter.log("Tost Message is - > " + jsonObj.getString("Toast"), true);
		return jsonObj.getString("Toast");
		}
		else {
			Reporter.log("Error message is not there.", true);
			return null;
		}
		
	}
	
	public String gettingUserId () throws JSONException
	{
		String text = driver.findElement(By.id("com.opentalk.staging:id/btn_automation")).getText();
		JSONObject jsonObj= new JSONObject(text);
		if (jsonObj.has("User Id"))
		{
		Reporter.log("User Id is - > " + jsonObj.getString("User Id"), true);
		this.userId = jsonObj.getString("User Id");
		return userId;
		}
		else {
			Reporter.log("User Id is not there.", true);
			return null;
		}
		
	}
	
	public String gettingCallId () throws JSONException
	{
		String text = driver.findElement(By.id("com.opentalk.staging:id/btn_automation")).getText();
		JSONObject jsonObj= new JSONObject(text);
		if (jsonObj.has("Call_id"))
		{
		Reporter.log("Call Id is - > " + jsonObj.getString("Call_id"), true);
		this.callId = jsonObj.getString("Call_id");
		return callId;
				
		}
		else {
			Reporter.log("Call Id is not there.", true);
			return null;
		}
		
	}
	

	
   public OpenTalkApp clicksOnMyTalkCircle()
   {
	    Reporter.log("Clicking on Home button.", true);
		List <WebElement> network = driver.findElements(By.id(properties.getPropertyValue("appId")+":id/bottom_navigation_small_container"));
		network.get(2).click();
	    Reporter.log("Selecting MyTalk circle.", true);
		driver.findElement(By.id(properties.getPropertyValue("appId")+":id/txt_selected_topic")).click();
		List <WebElement> circles = driver.findElements(By.className("android.widget.LinearLayout"));
		circles.get(0).click();
	   return this;
   }
   
   public boolean isUserOnMyTalkCircle()
   {
		try{
		String myTalkCircle =	driver.findElement(By.id(properties.getPropertyValue("appId")+":id/txt_selected_topic")).getText();
			Assert.assertTrue(myTalkCircle.contains("My Talk Circle"));
		    return true;
			
			}catch(Exception e)
			{
				Reporter.log(e.getMessage(), true);
				return false;
				}
   }

	public Map<String, String> getUserInfoOnMyTalkCircle(){
		Map<String, String> userInfo = new HashMap<>();
		Reporter.log("Getting connecting username.", true);
		List <WebElement> myTalkCircleUsers = driver.findElements(By.id(properties.getPropertyValue("appId")+":id/tv_name"));
		String myTalkCircleUser = myTalkCircleUsers.get(0).getText();
		Reporter.log(myTalkCircleUser, true);


		Reporter.log("Getting opentalks.", true);
		List <WebElement> myTalkCircleUsersOpentalks = driver.findElements(By.id(properties.getPropertyValue("appId")+":id/tv_talks"));
		String myTalkCircleUsersOpentalk = myTalkCircleUsersOpentalks.get(0).getText();
		Reporter.log(myTalkCircleUsersOpentalk, true);
		
		Reporter.log("Getting the Location.", true);			
		List <WebElement> myTalkCircleUsersLocations = driver.findElements(By.id(properties.getPropertyValue("appId")+":id/tv_location"));
		String myTalkCircleUsersLocation = myTalkCircleUsersLocations.get(0).getText();
		Reporter.log(myTalkCircleUsersLocation, true);
		
		Reporter.log("Getting the Request to Talk text.", true);			
		List <WebElement> myTalkCircleRequestToTalks = driver.findElements(By.id(properties.getPropertyValue("appId")+":id/tv_talk_type"));
		String myTalkCircleRequestToTalk = myTalkCircleRequestToTalks.get(0).getText();
		Reporter.log(myTalkCircleRequestToTalk, true);


		userInfo.put("userName", myTalkCircleUser);
		userInfo.put("openTalks", myTalkCircleUsersOpentalk);
		userInfo.put("location", myTalkCircleUsersLocation);
		userInfo.put("text", myTalkCircleRequestToTalk);
		
		return userInfo;
	}
	
	public OpenTalkApp clicksOnCreateTalkRequestIcon(){
		
		Reporter.log("User clicking on + icon.", true);
		List <WebElement> myTalkCircleRequestToTalkIcons = driver.findElements(By.id(properties.getPropertyValue("appId")+":id/ll_time"));
		myTalkCircleRequestToTalkIcons.get(0).click();
		
		return this;
	}
	public boolean isRequestToTalkScreen ()
	{
		try{
			Assert.assertTrue(isElementPresent(By.id(properties.getPropertyValue("appId")+":id/tv_open_to_talk")));
		    return true;
			
			}catch(Exception e)
			{
				Reporter.log(e.getMessage(), true);;
				return false;
				}
		
	}
	
	public OpenTalkApp createTalkRequest(){
		
		Reporter.log("Adding a note.", true);
		wait.until(
				 ExpectedConditions.visibilityOfElementLocated(By.id(
						 properties.getPropertyValue("appId")+":id/edt_note"))).sendKeys(Constant.firstUserTalkRequestNote);
		Reporter.log("Clicking on the create button.", true);
		wait.until(ExpectedConditions.elementToBeClickable(By.id(
				 properties.getPropertyValue("appId")+":id/action_button"))) .click();
		
		return this;
	}
	
	public OpenTalkApp clicksOnSentReceivedIcon() throws InterruptedException
	{

		Reporter.log("User clicking on Talk request icon.", true);
		List <WebElement> talkRequestIcon = driver.findElements(By.id(properties.getPropertyValue("appId")+":id/bottom_navigation_small_container"));
		talkRequestIcon.get(1).click();
		
		if (isElementPresent(By.id("android:id/button2"))){
			Reporter.log("User clicking on the No button.", true);
			wait.until(ExpectedConditions.elementToBeClickable(By.id(
					 "android:id/button2"))) .click();
			}
		return this;
	}
   
	public boolean isSentReceivedRequestScreen() throws InterruptedException
	{
		if (isElementPresent(MobileBy.xpath("//*[@class='android.widget.Button'][2]"))){
			Reporter.log("User clicking on the No button.", true);
			while (driver.findElements(MobileBy.xpath("//*[@class='android.widget.Button'][1]")).size()>0) 

			 {  driver.findElement(MobileBy.xpath("//*[@class='android.widget.Button'][1]")).click();
			 }
			}
		if (isElementPresent(By.id("android:id/button2"))){
			Reporter.log("User clicking on the No button.", true);
			wait.until(ExpectedConditions.elementToBeClickable(By.id(
					 "android:id/button2"))) .click();
			}


		try{
			Assert.assertTrue(isElementPresent(By.id(properties.getPropertyValue("appId")+":id/edt_search")));
		    return true;
			
			}catch(Exception e)
			{
				Reporter.log(e.getMessage(), true);;
				return false;
				}
		
	}
	
	public OpenTalkApp clicksOnSentTab(){
		
		Reporter.log("User clicking on Sent tab.", true);
		List <WebElement> tab = driver.findElement(By.id(properties.getPropertyValue("appId")+":id/tl_tabLayout")).findElements(By.className("android.support.v7.app.ActionBar$Tab"));
		tab.get(1).click();
		
		return this;
	}
	
	public Map<String, String> getTalkRequestInfoOnSent(){
		Map<String, String> userInfo = new HashMap<>();
		Reporter.log("Getting username.", true);
		String sentTalkRequestUser = driver.findElement(By.id(properties.getPropertyValue("appId")+":id/rv_talk_request_sent")).findElement(By.id(properties.getPropertyValue("appId")+":id/tv_name")).getText();
		Reporter.log("user name is -> " + sentTalkRequestUser, true);

		Reporter.log("Getting circle name.", true);
		String sentTalkRequestcircle = driver.findElement(By.id(properties.getPropertyValue("appId")+":id/rv_talk_request_sent")).findElement(By.id(properties.getPropertyValue("appId")+":id/txt_group_name")).getText();
		Reporter.log("circle name is -> " + sentTalkRequestcircle, true);


		userInfo.put("userName", sentTalkRequestUser);
		userInfo.put("circleName", sentTalkRequestcircle);
		
		return userInfo;
	}
	
	public Map<String, String> getTalkRequestInfoOnReceived(){
		Map<String, String> userInfo = new HashMap<>();
		Reporter.log("Getting username.", true);
		List <WebElement> receviedRequestUsers = driver.findElements(By.id(properties.getPropertyValue("appId")+":id/tv_name"));
		String receviedRequestUser = receviedRequestUsers.get(0).getText();
		Reporter.log("user name is -> " + receviedRequestUser, true);

		Reporter.log("Getting circle name.", true);
		List <WebElement> receviedRequestcircles = driver.findElements(By.id(properties.getPropertyValue("appId")+":id/txt_group_name"));
		String receviedRequestcircle = receviedRequestcircles.get(0).getText();
		Reporter.log("circle name is -> " + receviedRequestcircle, true);
		
		Reporter.log("Getting location.", true);
		List <WebElement> receviedRequesttopics = driver.findElements(By.id(properties.getPropertyValue("appId")+":id/tv_location"));
		String receviedRequesttopic = receviedRequesttopics.get(0).getText();
		Reporter.log("Location is -> " + receviedRequesttopic, true);


		userInfo.put("userName", receviedRequestUser);
		userInfo.put("circleName", receviedRequestcircle);
		userInfo.put("location", receviedRequesttopic);
		
		return userInfo;
	}
	
	public OpenTalkApp clicksOnCallIcon()
	{
		Reporter.log("User clicking on the call icon.", true);
		List <WebElement> callIcon = driver.findElements(By.id(properties.getPropertyValue("appId")+":id/ll_time"));
		callIcon.get(0).click();
		return this;
	}
	
	public OpenTalkApp clicksOnMyTalkStatusBar()
	{
		
		try{
		if (driver.findElement(By.id(properties.getPropertyValue("appId")+":id/btn_create_talk_status")).isDisplayed())
		{
		Reporter.log("Clicking on create talk request button.", true);
		wait.until(ExpectedConditions.elementToBeClickable(By.id(properties.getPropertyValue("appId")+":id/btn_create_talk_status")))
		.click();
		}}catch(Exception talk){	
			Reporter.log("Clicking on update talk request button.", true);
			wait.until(ExpectedConditions.elementToBeClickable(By.id(properties.getPropertyValue("appId")+":id/txt_update_status")))
			.click();}
		return this;
	}
	
	public boolean isCreatTalkStatusScreen()
	{
		try{
			Assert.assertTrue(isElementPresent(By.id(properties.getPropertyValue("appId")+":id/edt_talk_status")));
		    return true;
			
			}catch(Exception e)
			{
				Reporter.log(e.getMessage(), true);;
				return false;
				}
	}
	
	public OpenTalkApp creatTalkStatus()
	{
		Reporter.log("Adding a status.", true);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(properties.getPropertyValue("appId")+":id/edt_talk_status"))).clear();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(properties.getPropertyValue("appId")+":id/edt_talk_status"))).sendKeys(Constant.firstUserTalkStatus);
		
		Reporter.log("Clicking on the submit button.", true);
		wait.until(ExpectedConditions.elementToBeClickable(By.id(properties.getPropertyValue("appId")+":id/action_button")))
		.click();
		return this;
	}
   public String getTalkStatus ()
   {
	   String talkStatus = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(properties.getPropertyValue("appId")+":id/txt_talk_status"))).getText();
	   Reporter.log("Talk Status -> " + talkStatus , true);
	   return talkStatus;
   }
   
	public Map<String, String> getTalkStatusInfoOnSent(){
		Map<String, String> userInfo = new HashMap<>();
		Reporter.log("Facting the circle name.", true);
		String circleName = driver.findElement(By.id(properties.getPropertyValue("appId")+":id/rv_talk_request_sent")).findElement(By.id(properties.getPropertyValue("appId")+":id/tv_name")).getText();
		Reporter.log("circle name is -> " + circleName, true);
		
		Reporter.log("Facting the talk status.", true);
		String talkStatusInSent = driver.findElement(By.id(properties.getPropertyValue("appId")+":id/rv_talk_request_sent")).findElement(By.id(properties.getPropertyValue("appId")+":id/tv_tag")).getText();
		Reporter.log("Talk status is -> " + talkStatusInSent, true);


		userInfo.put("talkStatus", talkStatusInSent);
		userInfo.put("circleName", circleName);
		
		return userInfo;
	}
	public Map<String, String> getTalkStatusInfoOnCircleMemberList(){
		Map<String, String> userInfo = new HashMap<>();
		Reporter.log("Facting the group name.", true);
		List <WebElement> circles2UserGroupname = driver.findElements(By.id(properties.getPropertyValue("appId")+":id/txt_group_name"));
		String memberlistUserGroup = circles2UserGroupname.get(0).getText();
		Reporter.log("Member list user group name is - > " + memberlistUserGroup , true );
		
		Reporter.log("Facting the talk status.", true);
		List <WebElement> circles2Userstatus = driver.findElements(By.id(properties.getPropertyValue("appId")+":id/tv_talk_status"));
		String memberlistUserStatus = circles2Userstatus.get(0).getText();
		Reporter.log("Member list other user status is - > " + memberlistUserStatus , true );
		
		Reporter.log("Facting user name.", true);
		List <WebElement> circles2Username = driver.findElements(By.id(properties.getPropertyValue("appId")+":id/tv_name"));
		String memberlistUsername = circles2Username.get(0).getText();
		Reporter.log("Member list user name - > " + memberlistUsername , true );


		userInfo.put("talkStatus", memberlistUserStatus);
		userInfo.put("circleName", memberlistUserGroup);
		userInfo.put("userName", memberlistUsername);
		
		return userInfo;
	}
	
	
	public OpenTalkApp clickOnStatusCallIcon()
	{
		Reporter.log("User clicking on the call icon.", true);
		List <WebElement> talkStatusCallIcon = driver.findElements(By.id(properties.getPropertyValue("appId")+":id/ll_time"));
		talkStatusCallIcon.get(0).click();
		return this;
	}
	
	public void allowAppPermission(){
		 while (driver.findElements(MobileBy.xpath("//*[@class='android.widget.Button'][2]")).size()>0) 

		 {  driver.findElement(MobileBy.xpath("//*[@class='android.widget.Button'][2]")).click();
		 }
		}
	
	public void doBackground ()
	{
		CompletableFuture.runAsync(() -> { ((AppiumDriver)driver).runAppInBackground(Duration.ofSeconds(10)); });
		
		//((AppiumDriver)driver).runAppInBackground(Duration.ofSeconds(10));
	}
	
   
}
