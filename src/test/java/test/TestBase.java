package test;

import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONException;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import com.removeUserPojo.RemoveUserResponse;

import retrofit2.Call;
import retrofit2.Response;


public class TestBase {
	
	AppiumServerJava firstServer = new AppiumServerJava ();
	AppiumServerJava secondServer = new AppiumServerJava ();
	public AppProperties properties;
	
	
	@Parameters({ "environment", "appNameForFirstUser", "appNameForSecondUser","appPackage","udidForFirstUser", "udidForSecondUser" })
	@BeforeTest
	public void OpenApp(String env, String appNameForFirstUser ,String appNameForSecondUser, String appPackage , String udidForFirstUser, String udidForSecondUser ) throws MalformedURLException, InterruptedException
	{
		// Will Start the server in both
		firstServer.startAppiumServer(Constant.firstUserServerIp, Constant.firstUserServerPort);
		secondServer.startAppiumServer(Constant.secondUserServerIp, Constant.secondUserServerPort);
		
		// Setting the properties
		try {
			this.properties = AppProperties.getInstance(env);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			
		OpenTalkApp first = new OpenTalkApp(appNameForFirstUser, "6.0", Constant.firstUserServerIp, Constant.firstUserServerPort,  "Moto G", appPackage, "com.opentalk.activities.LoginActivity", udidForFirstUser);
		first.setAppProperties(properties);
		OpenTalkApp second = new OpenTalkApp(appNameForSecondUser,"5.0", Constant.secondUserServerIp, Constant.secondUserServerPort, "Micromax",appPackage, "com.opentalk.activities.LoginActivity",udidForSecondUser );
		second.setAppProperties(properties);
 		
		//Setting the users
		OpenTalkUsers.getInstance().setPrimaryUser(first);
		OpenTalkUsers.getInstance().setSecondryUser(second);
		
		
	    //SingIn or SignUp with FB
		OpenTalkUsers.getInstance().getPrimaryUser().doFBSignIn(properties.getPropertyValue("firstUserEmail"), properties.getPropertyValue("firstUserPassword"), properties.getPropertyValue("firstUserLanguage"));
		OpenTalkUsers.getInstance().getSecondryUser().doFBSignIn(properties.getPropertyValue("secondUserEmail"), properties.getPropertyValue("secondUserPassword"), properties.getPropertyValue("secondUserLanguage"));
	}
	
	
/*	@BeforeMethod
	public void beforeTestAppLogs() throws JSONException
	{
		OpenTalkUsers.getInstance().getPrimaryUser().gettingAppLogs();
	//	OpenTalkUsers.getInstance().getSecondryUser().gettingAppLogs();
	}*/
	
/*	@AfterMethod
	public void afterTestAppLogs() throws JSONException
	{
		OpenTalkUsers.getInstance().getPrimaryUser().gettingErrorLogs();
		OpenTalkUsers.getInstance().getSecondryUser().gettingErrorLogs();
	}*/
	
	@AfterTest
	public void closeApp() throws JSONException
	{
		/*Reporter.log("Verify the response of remove user API.", true);
		RetrofitService service = ServiceGenerator.createService(RetrofitService.class, properties.getPropertyValue("baseUrl"));
		Reporter.log("Base URL is " + properties.getPropertyValue("baseUrl"), true);
		
	    Call<RemoveUserResponse> call = service.removeUser(OpenTalkUsers.getInstance().getPrimaryUser().gettingUserId());
	    Call<RemoveUserResponse> call1 = service.removeUser(OpenTalkUsers.getInstance().getSecondryUser().gettingUserId());
		try {
			Response<RemoveUserResponse> response = call.execute();
			Response<RemoveUserResponse> response1 = call1.execute();
			Reporter.log("respone - " + response.body().getCode(), true);
			Reporter.log("respone - " + response1.body().getCode(), true);
			Assert.assertTrue(response.body().getCode().equals(200));
			Assert.assertTrue(response1.body().getCode().equals(200));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/

		OpenTalkUsers.getInstance().getPrimaryUser().quit();
		OpenTalkUsers.getInstance().getSecondryUser().quit();
		firstServer.stopServer();
		secondServer.stopServer();
	}

}
