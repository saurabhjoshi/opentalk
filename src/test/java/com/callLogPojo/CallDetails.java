
package com.callLogPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CallDetails {

    @SerializedName("version")
    @Expose
    private Long version;
    @SerializedName("call_id")
    @Expose
    private String callId;
    @SerializedName("talk_time")
    @Expose
    private Long talkTime;
    @SerializedName("created_at")
    @Expose
    private Long createdAt;
    @SerializedName("updated_at")
    @Expose
    private Long updatedAt;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("call_type")
    @Expose
    private String callType;
    @SerializedName("receiver_user_id")
    @Expose
    private Long receiverUserId;
    @SerializedName("request_user_id")
    @Expose
    private Long requestUserId;
    @SerializedName("receiver_topic_id")
    @Expose
    private Long receiverTopicId;
    @SerializedName("request_topic_id")
    @Expose
    private Long requestTopicId;
    @SerializedName("topic_id")
    @Expose
    private Long topicId;
    @SerializedName("disconnect_by")
    @Expose
    private Long disconnectBy;
    @SerializedName("disconnect_reason")
    @Expose
    private Long disconnectReason;
    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("status")
    @Expose
    private Long status;
    @SerializedName("unique_id")
    @Expose
    private String uniqueId;
    @SerializedName("opentok_call_id")
    @Expose
    private String opentokCallId;
    @SerializedName("topic_id_json")
    @Expose
    private String topicIdJson;
    @SerializedName("req_talk_time")
    @Expose
    private Long reqTalkTime;
    @SerializedName("request_user_es")
    @Expose
    private Long requestUserEs;
    @SerializedName("receiver_user_es")
    @Expose
    private Long receiverUserEs;
    @SerializedName("rating_update_status")
    @Expose
    private Long ratingUpdateStatus;
    @SerializedName("req_dc_reason")
    @Expose
    private Long reqDcReason;

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getCallId() {
        return callId;
    }

    public void setCallId(String callId) {
        this.callId = callId;
    }

    public Long getTalkTime() {
        return talkTime;
    }

    public void setTalkTime(Long talkTime) {
        this.talkTime = talkTime;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    public Long getReceiverUserId() {
        return receiverUserId;
    }

    public void setReceiverUserId(Long receiverUserId) {
        this.receiverUserId = receiverUserId;
    }

    public Long getRequestUserId() {
        return requestUserId;
    }

    public void setRequestUserId(Long requestUserId) {
        this.requestUserId = requestUserId;
    }

    public Long getReceiverTopicId() {
        return receiverTopicId;
    }

    public void setReceiverTopicId(Long receiverTopicId) {
        this.receiverTopicId = receiverTopicId;
    }

    public Long getRequestTopicId() {
        return requestTopicId;
    }

    public void setRequestTopicId(Long requestTopicId) {
        this.requestTopicId = requestTopicId;
    }

    public Long getTopicId() {
        return topicId;
    }

    public void setTopicId(Long topicId) {
        this.topicId = topicId;
    }

    public Long getDisconnectBy() {
        return disconnectBy;
    }

    public void setDisconnectBy(Long disconnectBy) {
        this.disconnectBy = disconnectBy;
    }

    public Long getDisconnectReason() {
        return disconnectReason;
    }

    public void setDisconnectReason(Long disconnectReason) {
        this.disconnectReason = disconnectReason;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getOpentokCallId() {
        return opentokCallId;
    }

    public void setOpentokCallId(String opentokCallId) {
        this.opentokCallId = opentokCallId;
    }

    public String getTopicIdJson() {
        return topicIdJson;
    }

    public void setTopicIdJson(String topicIdJson) {
        this.topicIdJson = topicIdJson;
    }

    public Long getReqTalkTime() {
        return reqTalkTime;
    }

    public void setReqTalkTime(Long reqTalkTime) {
        this.reqTalkTime = reqTalkTime;
    }

    public Long getRequestUserEs() {
        return requestUserEs;
    }

    public void setRequestUserEs(Long requestUserEs) {
        this.requestUserEs = requestUserEs;
    }

    public Long getReceiverUserEs() {
        return receiverUserEs;
    }

    public void setReceiverUserEs(Long receiverUserEs) {
        this.receiverUserEs = receiverUserEs;
    }

    public Long getRatingUpdateStatus() {
        return ratingUpdateStatus;
    }

    public void setRatingUpdateStatus(Long ratingUpdateStatus) {
        this.ratingUpdateStatus = ratingUpdateStatus;
    }

    public Long getReqDcReason() {
        return reqDcReason;
    }

    public void setReqDcReason(Long reqDcReason) {
        this.reqDcReason = reqDcReason;
    }

}
