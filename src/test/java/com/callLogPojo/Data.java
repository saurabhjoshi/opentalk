
package com.callLogPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("call_details")
    @Expose
    private CallDetails callDetails;

    public CallDetails getCallDetails() {
        return callDetails;
    }

    public void setCallDetails(CallDetails callDetails) {
        this.callDetails = callDetails;
    }

}
