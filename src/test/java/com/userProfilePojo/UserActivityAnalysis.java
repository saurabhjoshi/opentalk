
package com.userProfilePojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserActivityAnalysis {

    @SerializedName("userId")
    @Expose
    private Integer userId;
    @SerializedName("total_talktime")
    @Expose
    private Integer totalTalktime;
    @SerializedName("total_call")
    @Expose
    private Integer totalCall;
    @SerializedName("thumb_up")
    @Expose
    private Integer thumbUp;
    @SerializedName("thumb_down")
    @Expose
    private Integer thumbDown;
    @SerializedName("1_minutes_of_call")
    @Expose
    private Integer _1MinutesOfCall;
    @SerializedName("5_minutes_of_call")
    @Expose
    private Integer _5MinutesOfCall;
    @SerializedName("total_cookies_earned")
    @Expose
    private Integer totalCookiesEarned;
    @SerializedName("total_gems_used")
    @Expose
    private Integer totalGemsUsed;
    @SerializedName("minutes_of_call")
    @Expose
    private Object minutesOfCall;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getTotalTalktime() {
        return totalTalktime;
    }

    public void setTotalTalktime(Integer totalTalktime) {
        this.totalTalktime = totalTalktime;
    }

    public Integer getTotalCall() {
        return totalCall;
    }

    public void setTotalCall(Integer totalCall) {
        this.totalCall = totalCall;
    }

    public Integer getThumbUp() {
        return thumbUp;
    }

    public void setThumbUp(Integer thumbUp) {
        this.thumbUp = thumbUp;
    }

    public Integer getThumbDown() {
        return thumbDown;
    }

    public void setThumbDown(Integer thumbDown) {
        this.thumbDown = thumbDown;
    }

    public Integer get1MinutesOfCall() {
        return _1MinutesOfCall;
    }

    public void set1MinutesOfCall(Integer _1MinutesOfCall) {
        this._1MinutesOfCall = _1MinutesOfCall;
    }

    public Integer get5MinutesOfCall() {
        return _5MinutesOfCall;
    }

    public void set5MinutesOfCall(Integer _5MinutesOfCall) {
        this._5MinutesOfCall = _5MinutesOfCall;
    }

    public Integer getTotalCookiesEarned() {
        return totalCookiesEarned;
    }

    public void setTotalCookiesEarned(Integer totalCookiesEarned) {
        this.totalCookiesEarned = totalCookiesEarned;
    }

    public Integer getTotalGemsUsed() {
        return totalGemsUsed;
    }

    public void setTotalGemsUsed(Integer totalGemsUsed) {
        this.totalGemsUsed = totalGemsUsed;
    }

    public Object getMinutesOfCall() {
        return minutesOfCall;
    }

    public void setMinutesOfCall(Object minutesOfCall) {
        this.minutesOfCall = minutesOfCall;
    }

}
