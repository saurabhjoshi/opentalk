
package com.userProfilePojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LanguageMap {

    @SerializedName("Adja")
    @Expose
    private String adja;

    public String getAdja() {
        return adja;
    }

    public void setAdja(String adja) {
        this.adja = adja;
    }

}
