
package com.userProfilePojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("user_activity_analysis")
    @Expose
    private UserActivityAnalysis userActivityAnalysis;
    @SerializedName("user")
    @Expose
    private User user;

    public UserActivityAnalysis getUserActivityAnalysis() {
        return userActivityAnalysis;
    }

    public void setUserActivityAnalysis(UserActivityAnalysis userActivityAnalysis) {
        this.userActivityAnalysis = userActivityAnalysis;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
