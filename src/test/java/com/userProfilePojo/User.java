
package com.userProfilePojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("displayLanguage")
    @Expose
    private String displayLanguage;
    @SerializedName("favourite")
    @Expose
    private Boolean favourite;
    @SerializedName("languageMap")
    @Expose
    private LanguageMap languageMap;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("level")
    @Expose
    private Integer level;
    @SerializedName("display_location")
    @Expose
    private String displayLocation;
    @SerializedName("is_favourite")
    @Expose
    private Boolean isFavourite;
    @SerializedName("can_chat")
    @Expose
    private Boolean canChat;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDisplayLanguage() {
        return displayLanguage;
    }

    public void setDisplayLanguage(String displayLanguage) {
        this.displayLanguage = displayLanguage;
    }

    public Boolean getFavourite() {
        return favourite;
    }

    public void setFavourite(Boolean favourite) {
        this.favourite = favourite;
    }

    public LanguageMap getLanguageMap() {
        return languageMap;
    }

    public void setLanguageMap(LanguageMap languageMap) {
        this.languageMap = languageMap;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getDisplayLocation() {
        return displayLocation;
    }

    public void setDisplayLocation(String displayLocation) {
        this.displayLocation = displayLocation;
    }

    public Boolean getIsFavourite() {
        return isFavourite;
    }

    public void setIsFavourite(Boolean isFavourite) {
        this.isFavourite = isFavourite;
    }

    public Boolean getCanChat() {
        return canChat;
    }

    public void setCanChat(Boolean canChat) {
        this.canChat = canChat;
    }

}
