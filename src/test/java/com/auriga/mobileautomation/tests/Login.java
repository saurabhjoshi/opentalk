package com.auriga.mobileautomation.tests;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.StartsActivity;

public class Login {
	
	// This call having the code of mobile login.

	public static final String USERNAME = "OmNewAuriga";
	public static final String ACCESS_KEY = "de3c1dca-9d55-451d-b060-6511921dbcf1";
	public static final String URL = "http://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:80/wd/hub";
	// public static final String URL =
	// "http://"+System.getenv("SAUCE_USERNAME")+":"+System.getenv("SAUCE_ACCESS_KEY")+"@ondemand.saucelabs.com:80/wd/hub";
	public WebDriver driver;

	public WebDriver driver1;
	// public WebDriver driver2;

	WebDriverWait waitindriver;

	WebDriverWait waitindriver1;
	// WebDriverWait waitindriver2;

	
	
	@Test(priority = 0 )
	public void Verify_Users() throws InterruptedException, IOException {

/*		waitindriver = new WebDriverWait(driver, 30);
	//	waitindriver1 = new WebDriverWait(driver1, 30);

		Reporter.log("First user clicking on the Login button.", true);
		waitindriver.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/btn_fb_login")))
				.click();

		Reporter.log("Entering email.", true);
		List<WebElement> e = driver.findElements(By.className("android.widget.EditText"));
		e.get(0).sendKeys("saurabh.joshi@aurigait.com");
		Reporter.log("Clicking on the Login button.", true);
		waitindriver.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.Button"))).click();
		Thread.sleep(1000);
		List<WebElement> e1 = driver.findElements(By.className("android.widget.EditText"));
		if (e1.size() == 2) {
			Reporter.log("Entering password.", true);
			e1.get(1).sendKeys("testing123"); // test password
		} else {
			Reporter.log("Entering password.", true);
			e1.get(0).sendKeys("testing123");
		}
		Reporter.log("Clicking on the Login button.", true);
		waitindriver.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.Button"))).click();
		Thread.sleep(2000);
		Reporter.log("Clicking on the continue button.", true);
		waitindriver.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.Button"))).click();*/
		



		/*		// Second User Login

		Reporter.log("Second user clicking on the Login button.", true);
		waitindriver1.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/btn_fb_login")))
				.click();

		List<WebElement> e2 = driver1.findElements(By.className("android.widget.EditText"));
		Reporter.log("Entering email.", true);
		e2.get(0).sendKeys("sonupk2007@gmail.com");
		Thread.sleep(1000);
		Reporter.log("Clicking on the Login button.", true);
		waitindriver1.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.Button"))).click();
		Reporter.log("Entering password.", true);
		List<WebElement> e3 = driver1.findElements(By.className("android.widget.EditText"));
		if (e3.size() == 2) {
			e3.get(1).sendKeys("sonupk2007(sonu)"); // test password
		} else {
			e3.get(0).sendKeys("sonupk2007(sonu)");
		}
		Reporter.log("Clicking on the Login button.", true);
		waitindriver1.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.Button"))).click();
		Thread.sleep(2000);
		Reporter.log("Clicking on the continue button.", true);
		waitindriver1.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.Button"))).click();

		// Verify First User

		Reporter.log("First User Clicking on Continue Button.", true);
		waitindriver.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/txt_done"))).click();

		Reporter.log("First user un selecting English language.", true);
		List<WebElement> rl_main = driver.findElements(By.id("com.opentalk.staging:id/rl_main"));
		rl_main.get(0).click();
		rl_main.get(1).click();
		rl_main.get(2).click();
		rl_main.get(3).click();

		Reporter.log("Clicking on the search icon of first user.", true);
		waitindriver.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/menu_search")))
				.click();

		Reporter.log("Entering language of the first user.", true);
		AndroidElement search = (AndroidElement) waitindriver
				.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/search_src_text")));
		search.sendKeys("Bali");
		Reporter.log("Entering language -> Bali", true);
		Reporter.log("Selecting language -> Bali", true);
		waitindriver.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/txt_language")))
				.click();

		Reporter.log("Clicking on submit button of first user.", true);
		List<WebElement> submit = driver.findElements(By.className("android.widget.ImageButton"));
		submit.get(1).click();

		Thread.sleep(2000);

		Reporter.log("Clicking on submit button of first user.", true);
		submit.get(1).click();
		
		String text = driver.findElement(By.id("com.opentalk.staging:id/btn_automation")).getText();
		Reporter.log(text,true);
		
		Reporter.log("Clicking on user's profile.", true);
		List <WebElement> userProfile = driver.findElements(By.id("com.opentalk.staging:id/bottom_navigation_small_container"));
		userProfile.get(2).click();

		Reporter.log("Factching the user name.", true);
		WebElement userName = driver.findElement(By.id("com.opentalk.staging:id/et_name"));
		System.out.println(userName.getText());

		Reporter.log("Verify the User Name.", true);
		Assert.assertTrue(userName.getText().contains("Saurabh"));

		// Verify Second User

		Reporter.log("Second User Clicking on Continue Button.", true);
		waitindriver1.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/txt_done"))).click();

		Reporter.log("Secound user un selecting English language.", true);
		List<WebElement> rl_main1 = driver1.findElements(By.id("com.opentalk.staging:id/rl_main"));
		rl_main1.get(0).click();
		rl_main1.get(1).click();
		rl_main1.get(2).click();
		rl_main1.get(3).click();
		// rl_main1.get(4).click();

		Reporter.log("Clicking on the search icon of first user.", true);
		waitindriver1.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/menu_search")))
				.click();

		Reporter.log("Entering language of the Second user.", true);
		AndroidElement search1 = (AndroidElement) waitindriver1
				.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/search_src_text")));
		search1.sendKeys("Bali");
		Reporter.log("Entering language -> Bali", true);
		Reporter.log("Selecting language -> Bali", true);
		waitindriver1
				.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/txt_language")))
				.click();

		Reporter.log("Clicking on submit button.", true);
		List<WebElement> submit1 = driver1.findElements(By.className("android.widget.ImageButton"));
		submit1.get(1).click();

		Thread.sleep(2000);

		Reporter.log("Clicking on submit button.", true);
		submit1.get(1).click();

		Reporter.log("Clicking on user's profile.", true);
		List <WebElement> secondUserProfile = driver1.findElements(By.id("com.opentalk.staging:id/bottom_navigation_small_container"));
		secondUserProfile.get(2).click();

		Reporter.log("Factching the user name.", true);
		WebElement userName1 = driver1.findElement(By.id("com.opentalk.staging:id/et_name"));
		System.out.println(userName1.getText());

		Reporter.log("Verify the User Name.", true);
		Assert.assertTrue(userName1.getText().contains("Purusottam"));

		// First User Talk now
		Reporter.log("Fatching logs.", true);
		String text10 = driver.findElement(By.id("com.opentalk.staging:id/btn_automation")).getText();
		Reporter.log(text10,true);

		Reporter.log("Clicking on Home button.", true);
		List <WebElement> homeIcon = driver.findElements(By.id("com.opentalk.staging:id/bottom_navigation_small_container"));
		homeIcon.get(0).click();

		Reporter.log("Selecting topic for the first user.", true);
		List<WebElement> childElements = driver.findElement(By.id("com.opentalk.staging:id/rv_topics"))
				.findElements(By.id("com.opentalk.staging:id/card_main"));
		WebElement first_User_Selected_Topic = childElements.get(0);
		String topic_User1 = first_User_Selected_Topic.findElement(By.id("com.opentalk.staging:id/txt_topic_name"))
				.getText();
		Reporter.log("First user's topic -> " + topic_User1, true);
		first_User_Selected_Topic.click();

		Reporter.log("Clicking on the Talk Now button of the first user.", true);
		waitindriver.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/iv_shape")))
				.click();
		Reporter.log("Initiative the call and finding the partners for the first user.", true);

		// Second User Talk Now
		
		Reporter.log("Fatching logs.", true);
		String text1 = driver1.findElement(By.id("com.opentalk.staging:id/btn_automation")).getText();
		Reporter.log(text1,true);

		Reporter.log("Clicking on Home button.", true);
		List <WebElement> homeIcon1 = driver1.findElements(By.id("com.opentalk.staging:id/bottom_navigation_small_container"));
		homeIcon1.get(0).click();

		Reporter.log("Selecting topic for the Second user.", true);
		List<WebElement> childElements1 = driver1.findElement(By.id("com.opentalk.staging:id/rv_topics"))
				.findElements(By.id("com.opentalk.staging:id/card_main"));
		WebElement second_User_Selected_Topic = childElements1.get(0);
		String topic_User2 = second_User_Selected_Topic.findElement(By.id("com.opentalk.staging:id/txt_topic_name"))
				.getText();
		Reporter.log("Second user's topic -> " + topic_User2, true);
		second_User_Selected_Topic.click();

		Reporter.log("Clicking on the Talk Now button of the Second user.", true);
		waitindriver1.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/iv_shape")))
				.click();
		Reporter.log("Initiative the call and finding the partners for the Second user.", true);

		//Thread.sleep(500);

		// First user's connecting screen

		Reporter.log("Verifying the connecting user name for the first user.", true);
		String connecting_username = waitindriver
				.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/txt_user_name")))
				.getText();
		Reporter.log("connecting_username -> " + connecting_username, true);
		Assert.assertEquals(connecting_username, "Purusottam");

		Reporter.log("Verifying the connecting user language for the first user.", true);
		String connecting_user_languages = waitindriver
				.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/txt_languages")))
				.getText();
		Reporter.log("connecting_user_languages -> " + connecting_user_languages, true);
		Assert.assertTrue(connecting_user_languages.contains("Bali"));

		Reporter.log("Verifying the connecting user topic for the first user.", true);
		String connecting_user_topic = waitindriver
				.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/txt_topic")))
				.getText();
		Reporter.log("connecting_user_topic -> " + connecting_user_topic, true);
		Assert.assertTrue(connecting_user_topic.contains(topic_User2));
		

		// First user's connected screen

		Reporter.log("Facting connected username for the first user.", true);
		String connected_username = waitindriver
				.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/txt_user_name")))
				.getText();
		Reporter.log("connected_username -> " + connected_username, true);
		Assert.assertEquals(connected_username, "Purusottam");

		Reporter.log("First user clicking on the extend call button.", true);
		waitindriver.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/tv_entend_call")))
				.click();
		Thread.sleep(5000);

		// Second user's connected screen

		Reporter.log("Facting connected username for the second user.", true);
		String connected_username1 = waitindriver1
				.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/txt_user_name")))
				.getText();
		Reporter.log("connected_username1 -> " + connected_username1, true);
		Assert.assertEquals(connected_username1, "Saurabh");

		Reporter.log("Second user clicking on the extend call button.", true);
		waitindriver1.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/tv_entend_call")))
				.click();
		Thread.sleep(5000);

		// First user connected screen

		Reporter.log("Fatching connected time.", true);
		String connected_time = waitindriver.until(
				ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/text_call_duration")))
				.getText();
		Reporter.log("Connected time -> " + connected_time, true);

		Reporter.log("Clicking on the stop connecting button.", true);
		waitindriver.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/iv_end_call")))
				.click();

		// First User's rate call screen

		Reporter.log("Facting connected username for the first user.", true);
		String rate_connected_username = waitindriver
				.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/txt_user_name")))
				.getText();
		Reporter.log("rate_connected_username -> " + rate_connected_username, true);
		Assert.assertEquals(rate_connected_username, "Purusottam");

		Reporter.log("Fatching connected time for the first user.", true);
		String rate_connected_time = waitindriver.until(
				ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/text_call_duration")))
				.getText();
		Reporter.log("Connected time on rate call screen for the first user -> " + rate_connected_time, true);

		Reporter.log("First user clicking on the thumbs up.", true);
		waitindriver.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/iv_thumbs_up")))
				.click();
		
		Reporter.log("Clicking on the submit button.", true);
	//	driver.findElement(By.id("com.opentalk.staging:id/card_submit"));
		waitindriver.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/card_submit")))
		.click();
		
		Reporter.log("First user clicking on the stop connecting," , true);
		driver.findElement(By.id("com.opentalk.staging:id/iv_stop_connecting")).click();

		// Second user's rate call screen

	
		 Reporter.log("Facting connected username for the second user.", true);
		 String rate_connected_username1 = waitindriver1
		 .until(ExpectedConditions.visibilityOfElementLocated(By.id(
		 "com.opentalk.staging:id/txt_user_name"))).getText(); 
		 Reporter.log(
		 "rate_connected_username1 -> " + rate_connected_username1, true);
		 Assert.assertEquals(rate_connected_username1, "Saurabh");
		  
		 Reporter.log("Fatching connected time for the second user.", true); String
		 rate_connected_time1 = waitindriver1.until(
		 ExpectedConditions.visibilityOfElementLocated(By.id(
		 "com.opentalk.staging:id/text_call_duration"))) .getText();
		 Reporter.log(
		 "Connected time on rate call screen for the second user -> " +
		 rate_connected_time1, true);
		 
		 Reporter.log("Second user clicking on the thumbs up.", true);
		 waitindriver1.until(ExpectedConditions.elementToBeClickable(By.id(
		 "com.opentalk.staging:id/iv_thumbs_up"))) .click();
		 
			Reporter.log("Clicking on the submit button," , true);
			//driver1.findElement(By.id("com.opentalk.staging:id/card_submit"));
			waitindriver1.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/card_submit")))
			.click();
		 
			Reporter.log("Second user clicking on the stop connecting.", true);
			driver1.findElement(By.id("com.opentalk.staging:id/iv_stop_connecting")).click();
		 

		
		

		
		// First user creates a request in My talk circle
		
		
		
		
			
			Reporter.log("Selecting MyTalk circle.", true);
			driver.findElement(By.id("com.opentalk.staging:id/txt_selected_topic")).click();
			List <WebElement> circles = driver.findElements(By.className("android.widget.LinearLayout"));
			circles.get(1).click();
			
			Reporter.log("Verifying the user in my talk circle.", true);			
			List <WebElement> myTalkCircleUsers = driver.findElements(By.id("com.opentalk.staging:id/tv_name"));
			String myTalkCircleUser = myTalkCircleUsers.get(0).getText();
			Reporter.log(myTalkCircleUser, true);
			Assert.assertTrue(myTalkCircleUser.contains("Purusottam"));
			
			Reporter.log("Verifying the opentalks.", true);			
			List <WebElement> myTalkCircleUsersOpentalks = driver.findElements(By.id("com.opentalk.staging:id/tv_talks"));
			String myTalkCircleUsersOpentalk = myTalkCircleUsersOpentalks.get(0).getText();
			Reporter.log(myTalkCircleUsersOpentalk, true);
			Assert.assertTrue(myTalkCircleUsersOpentalk.contains("Opentalks"));
			
			Reporter.log("Verifying the Location.", true);			
			List <WebElement> myTalkCircleUsersLocations = driver.findElements(By.id("com.opentalk.staging:id/tv_location"));
			String myTalkCircleUsersLocation = myTalkCircleUsersLocations.get(0).getText();
			Reporter.log(myTalkCircleUsersLocation, true);
			//Assert.assertTrue(myTalkCircleUsersLocation.contains("Opentalks"));
			
			Reporter.log("Verifying the Request to Talk text.", true);			
			List <WebElement> myTalkCircleRequestToTalks = driver.findElements(By.id("com.opentalk.staging:id/tv_talk_type"));
			String myTalkCircleRequestToTalk = myTalkCircleRequestToTalks.get(0).getText();
			Reporter.log(myTalkCircleRequestToTalk, true);
			Assert.assertTrue(myTalkCircleRequestToTalk.contains("Request To Talk"));
			
			Reporter.log("Verifying that by clicking on the + icon the user is redirected to the Request to Talk screen.", true);			
			List <WebElement> myTalkCircleRequestToTalkIcons = driver.findElements(By.id("com.opentalk.staging:id/ll_time"));
			myTalkCircleRequestToTalkIcons.get(0).click();
			String requestToTalk = waitindriver.until(
					 ExpectedConditions.visibilityOfElementLocated(By.id(
							 "com.opentalk.staging:id/tv_open_to_talk"))) .getText();
			Reporter.log(requestToTalk, true);
			Assert.assertTrue(requestToTalk.contains("I am open to talk To Purusottam"));
			
			Reporter.log("User going to creates a talk request.", true);
			Reporter.log("Adding a note.", true);
			waitindriver.until(
					 ExpectedConditions.visibilityOfElementLocated(By.id(
							 "com.opentalk.staging:id/edt_note"))).sendKeys("Test Note");
			Reporter.log("Clicking on the create button.", true);
			waitindriver.until(ExpectedConditions.elementToBeClickable(By.id(
					 "com.opentalk.staging:id/action_button"))) .click();
			
			Reporter.log("Verifying the Edit Request Text.", true);			
			List <WebElement> myTalkCircleEditRequests = driver.findElements(By.id("com.opentalk.staging:id/tv_talk_type"));
			String myTalkCircleEditRequest = myTalkCircleEditRequests.get(0).getText();
			Reporter.log(myTalkCircleEditRequest, true);
			Assert.assertTrue(myTalkCircleEditRequest.contains("Edit Request"));
			
			Reporter.log("User clicking on Talk request icon.", true);
			List <WebElement> talkRequestIcon = driver.findElements(By.id("com.opentalk.staging:id/bottom_navigation_small_container"));
			talkRequestIcon.get(1).click();
			
			try{
			if (driver.findElement(By.id("android:id/button2")).isDisplayed()){
			Reporter.log("User clicking on the No button.", true);
			waitindriver.until(ExpectedConditions.elementToBeClickable(By.id(
					 "android:id/button2"))) .click();
			}}
			catch(Exception error)
			{
				Reporter.log("User clicking on Sent tab.", true);
				List <WebElement> tab = driver.findElements(By.className("android.support.v7.app.ActionBar$Tab"));
				tab.get(1).findElement(By.id("com.opentalk.staging:id/tab_title")).click();
			}
			
			Reporter.log("User clicking on Sent tab.", true);
			List <WebElement> tab = driver.findElements(By.className("android.support.v7.app.ActionBar$Tab"));
			tab.get(1).findElement(By.id("com.opentalk.staging:id/tab_title")).click();
			
			Reporter.log("Verifying the Talk request in sent screen.", true);	
			
			String sentTalkRequestUser = driver.findElement(By.id("com.opentalk.staging:id/rv_talk_request_sent")).findElement(By.id("com.opentalk.staging:id/tv_name")).getText();
			Reporter.log("user name is -> " + sentTalkRequestUser, true);
			Reporter.log("Verifying the user name." , true);
			Assert.assertTrue(sentTalkRequestUser.contains("Purusottam"));
			
			Reporter.log("Verifying circle name.", true);

			String sentTalkRequestcircle = driver.findElement(By.id("com.opentalk.staging:id/rv_talk_request_sent")).findElement(By.id("com.opentalk.staging:id/txt_group_name")).getText();
			Reporter.log("circle name is -> " + sentTalkRequestcircle, true);
			Reporter.log("Verifying circle name." , true);
			Assert.assertTrue(sentTalkRequestcircle.contains("My Talk Circle"));
			
			
			// Second user talk request scrren
			
			Reporter.log("User clicking on Talk request icon.", true);
			List <WebElement> talkRequestIcon1 = driver1.findElements(By.id("com.opentalk.staging:id/bottom_navigation_small_container"));
			talkRequestIcon1.get(1).click();
			

			Reporter.log("Verifying the Talk request.", true);	
			Reporter.log("Verifying username.", true);
			List <WebElement> receviedRequestUsers = driver1.findElements(By.id("com.opentalk.staging:id/tv_name"));
			String receviedRequestUser = receviedRequestUsers.get(0).getText();
			
			Assert.assertTrue(receviedRequestUser.contains("Saurabh"));
			
			Reporter.log("Verifying circle name.", true);
			List <WebElement> receviedRequestcircles = driver1.findElements(By.id("com.opentalk.staging:id/txt_group_name"));
			String receviedRequestcircle = receviedRequestcircles.get(0).getText();
			Assert.assertTrue(receviedRequestcircle.contains("My Talk Circle"));
			
			Reporter.log("Verifying topic of the talk request.", true);
			List <WebElement> receviedRequesttopics = driver1.findElements(By.id("com.opentalk.staging:id/tv_talk_status"));
			String receviedRequesttopic = receviedRequesttopics.get(0).getText();
			Assert.assertTrue(receviedRequesttopic.contains("Wants to Reconnect"));
			
			Reporter.log("User clicking on the call icon.", true);
			List <WebElement> callIcon = driver1.findElements(By.id("com.opentalk.staging:id/ib_add_request"));
			callIcon.get(0).click();
			
			
			// first user connecting screen
			
			Reporter.log("Verifying the connecting user name for the first user.", true);
			String reconnectingUsername = waitindriver
					.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/txt_user_name")))
					.getText();
			Reporter.log("reconnecting Username -> " + reconnectingUsername, true);
			Assert.assertEquals(reconnectingUsername, "Purusottam");
			
			// First user's connected screen

			Reporter.log("Facting connected username for the first user.", true);
			String reconnectedUsername = waitindriver
					.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/txt_user_name")))
					.getText();
			Reporter.log("reconnected Username -> " + reconnectedUsername, true);
			Assert.assertEquals(reconnectedUsername, "Purusottam");

			Reporter.log("First user clicking on the extend call button.", true);
			waitindriver.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/tv_entend_call")))
					.click();
			
			Thread.sleep(5000);

			// Second user's connected screen

			Reporter.log("Facting connected username for the second user.", true);
			String reconnectedUsername1 = waitindriver1
					.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/txt_user_name")))
					.getText();
			Reporter.log("reconnected Username1 -> " + reconnectedUsername1, true);
			Assert.assertEquals(reconnectedUsername1, "Saurabh");

			Reporter.log("Second user clicking on the extend call button.", true);
			waitindriver1.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/tv_entend_call")))
					.click();
			Thread.sleep(5000);

			// First user connected screen

			Reporter.log("Fatching connected time.", true);
			String reconnectedTime = waitindriver.until(
					ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/text_call_duration")))
					.getText();
			Reporter.log("Connected time -> " + reconnectedTime, true);

			Reporter.log("Clicking on the stop connecting button.", true);
			waitindriver.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/iv_end_call")))
					.click();

			// First User's rate call screen

			Reporter.log("Facting connected username for the first user.", true);
			String rateReconnectedUsername = waitindriver
					.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/txt_user_name")))
					.getText();
			Reporter.log("rate Reconnected Username -> " + rateReconnectedUsername, true);
			Assert.assertEquals(rateReconnectedUsername, "Purusottam");

			Reporter.log("Fatching connected time for the first user.", true);
			String rateReconnectedTime = waitindriver.until(
					ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/text_call_duration")))
					.getText();
			Reporter.log("Connected time on rate call screen for the first user -> " + rateReconnectedTime, true);

			Reporter.log("First user clicking on the thumbs up.", true);
			waitindriver.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/iv_thumbs_up")))
					.click();
			
			Reporter.log("Clicking on the submit button." , true);
			waitindriver.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/card_submit")))
			.click();
			

			// Second user's rate call screen

		
			 Reporter.log("Facting connected username for the second user.", true);
			 String rateReconnectedUsername1 = waitindriver1
			 .until(ExpectedConditions.visibilityOfElementLocated(By.id(
			 "com.opentalk.staging:id/txt_user_name"))).getText(); 
			 Reporter.log(
			 "rate Reconnected Username1 -> " + rateReconnectedUsername1, true);
			 Assert.assertEquals(rateReconnectedUsername1, "Saurabh");
			  
			 Reporter.log("Fatching connected time for the second user.", true); String
			 rateReconnectedTime1 = waitindriver1.until(
			 ExpectedConditions.visibilityOfElementLocated(By.id(
			 "com.opentalk.staging:id/text_call_duration"))) .getText();
			 Reporter.log(
			 "Connected time on rate call screen for the second user -> " +
					 rateReconnectedTime1, true);
			 
			 Reporter.log("Second user clicking on the thumbs up.", true);
			 waitindriver1.until(ExpectedConditions.elementToBeClickable(By.id(
			 "com.opentalk.staging:id/iv_thumbs_up"))) .click();
			 
				Reporter.log("Clicking on the submit button." , true);
				waitindriver1.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/card_submit")))
				.click();
			 
		
				
				
				
				//  First user going to create a talk status
				
				Reporter.log("Clicking on Home button.", true);
				List <WebElement> homeIcon10 = driver.findElements(By.id("com.opentalk.staging:id/bottom_navigation_small_container"));
				homeIcon10.get(0).click();
				
				Reporter.log("Selecting MyTalk circle.", true);
				driver.findElement(By.id("com.opentalk.staging:id/txt_selected_topic")).click();
				List <WebElement> circles1 = driver.findElements(By.className("android.widget.LinearLayout"));
				circles1.get(1).click();
				
				Reporter.log("Clicking on the talk status bar.", true);
				try{
				if (driver.findElement(By.id("com.opentalk.staging:id/txt_set_talk_status")).isDisplayed())
				{
				waitindriver.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/txt_set_talk_status")))
				.click();
				}}catch(Exception talk){	waitindriver.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/txt_talk_status")))
					.click();}
				
				
				Reporter.log("Adding a status.", true);
				waitindriver.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/edt_talk_status"))).clear();
				waitindriver.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/edt_talk_status"))).sendKeys("Test Status");
				
				Reporter.log("Clicking on the submit button.", true);
				waitindriver.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/action_button")))
				.click();
				
				Reporter.log("Verifying the talk status.", true);
				String talkStatus = waitindriver.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/txt_talk_status"))).getText();
				Reporter.log("Talk Status -> " + talkStatus , true);
				Assert.assertTrue(talkStatus.contains("Test Status"));
				
				Reporter.log("Verifying the talk status in sent requests.", true);
				Reporter.log("User clicking on Talk request icon.", true);
				List <WebElement> talkRequestIcon2 = driver.findElements(By.id("com.opentalk.staging:id/bottom_navigation_small_container"));
				talkRequestIcon2.get(1).click();
				
				try{
					if (driver.findElement(By.id("android:id/button2")).isDisplayed()){
					Reporter.log("User clicking on the No button.", true);
					waitindriver.until(ExpectedConditions.elementToBeClickable(By.id(
							 "android:id/button2"))) .click();
					}}
					catch(Exception error)
					{
						Reporter.log("User clicking on Sent tab.", true);
						List <WebElement> tab2 = driver.findElements(By.className("android.support.v7.app.ActionBar$Tab"));
						tab2.get(1).findElement(By.id("com.opentalk.staging:id/tab_title")).click();
					}
				
				Reporter.log("User clicking on Sent tab.", true);
				List <WebElement> tab1 = driver.findElements(By.className("android.support.v7.app.ActionBar$Tab"));
				tab1.get(1).findElement(By.id("com.opentalk.staging:id/tab_title")).click();
				
				Reporter.log("Facting the circle name.", true);
				String circleName = driver.findElement(By.id("com.opentalk.staging:id/rv_talk_request_sent")).findElement(By.id("com.opentalk.staging:id/tv_name")).getText();
				Reporter.log("circle name is -> " + circleName, true);
				Reporter.log("Verifying circle name." , true);
				Assert.assertTrue(circleName.contains("My Talk Circle"));
				
				Reporter.log("Facting the talk status.", true);
				String talkStatusInSent = driver.findElement(By.id("com.opentalk.staging:id/rv_talk_request_sent")).findElement(By.id("com.opentalk.staging:id/tv_talk_status")).getText();
				Reporter.log("Talk status is -> " + talkStatusInSent, true);
				Reporter.log("Verifying talk status." , true);
				Assert.assertTrue(talkStatusInSent.contains("Test Status"));
				
				// Second user
				
				Reporter.log("Clicking on Home button.", true);
				List <WebElement> homeIcon2 = driver1.findElements(By.id("com.opentalk.staging:id/bottom_navigation_small_container"));
				homeIcon2.get(0).click();
				
				Reporter.log("Selecting MyTalk circle.", true);
				driver1.findElement(By.id("com.opentalk.staging:id/txt_selected_topic")).click();
				List <WebElement> circles2 = driver1.findElements(By.className("android.widget.LinearLayout"));
				circles2.get(1).click();
				
				Reporter.log("Verifying user name.", true);
				List <WebElement> circles2Username = driver1.findElements(By.id("com.opentalk.staging:id/tv_name"));
				String memberlistUsername = circles2Username.get(0).getText();
				Reporter.log("Member list user name - > " + memberlistUsername , true );
				Assert.assertTrue(memberlistUsername.contains("Saurabh"));
				
				Reporter.log("Verifying group name.", true);
				List <WebElement> circles2UserGroupname = driver1.findElements(By.id("com.opentalk.staging:id/txt_group_name"));
				String memberlistUserGroup = circles2UserGroupname.get(0).getText();
				Reporter.log("Member list user group name is - > " + memberlistUserGroup , true );
				Assert.assertTrue(memberlistUserGroup.contains("My Talk Circle"));
				
				Reporter.log("Verifying talk status.", true);
				List <WebElement> circles2Userstatus = driver1.findElements(By.id("com.opentalk.staging:id/tv_talk_status"));
				String memberlistUserStatus = circles2Userstatus.get(0).getText();
				Reporter.log("Member list other user status is - > " + memberlistUserStatus , true );
				Assert.assertTrue(memberlistUserStatus.contains("Test Status"));
				
				Reporter.log("User clicking on the call icon.", true);
				List <WebElement> talkStatusCallIcon = driver1.findElements(By.id("com.opentalk.staging:id/ll_time"));
				talkStatusCallIcon.get(0).click();
				
				// first user connecting screen
				
				Reporter.log("Verifying the connecting user name for the first user.", true);
				String reconnectingUsername1 = waitindriver
						.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/txt_user_name")))
						.getText();
				Reporter.log("reconnecting Username -> " + reconnectingUsername1, true);
				Assert.assertEquals(reconnectingUsername1, "Purusottam");
				
				// First user's connected screen

				Reporter.log("Facting connected username for the first user.", true);
				String reconnectedUsername2 = waitindriver
						.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/txt_user_name")))
						.getText();
				Reporter.log("reconnected Username -> " + reconnectedUsername2, true);
				Assert.assertEquals(reconnectedUsername2, "Purusottam");

				Reporter.log("First user clicking on the extend call button.", true);
				waitindriver.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/tv_entend_call")))
						.click();
				Thread.sleep(5000);
				
				// Second user's connected screen

				Reporter.log("Facting connected username for the second user.", true);
				String reconnectedUsername3 = waitindriver1
						.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/txt_user_name")))
						.getText();
				Reporter.log("reconnected Username1 -> " + reconnectedUsername3, true);
				Assert.assertEquals(reconnectedUsername3, "Saurabh");

				Reporter.log("Second user clicking on the extend call button.", true);
				waitindriver1.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/tv_entend_call")))
						.click();
				Thread.sleep(5000);

				// First user connected screen

				Reporter.log("Fatching connected time.", true);
				String reconnectedTime1 = waitindriver.until(
						ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/text_call_duration")))
						.getText();
				Reporter.log("Connected time -> " + reconnectedTime1, true);

				Reporter.log("Clicking on the stop connecting button.", true);
				waitindriver.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/iv_end_call")))
						.click();

				// First User's rate call screen

				Reporter.log("Facting connected username for the first user.", true);
				String rateReconnectedUsername2 = waitindriver
						.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/txt_user_name")))
						.getText();
				Reporter.log("rate Reconnected Username -> " + rateReconnectedUsername2, true);
				Assert.assertEquals(rateReconnectedUsername2, "Purusottam");

				Reporter.log("Fatching connected time for the first user.", true);
				String rateReconnectedTime2 = waitindriver.until(
						ExpectedConditions.visibilityOfElementLocated(By.id("com.opentalk.staging:id/text_call_duration")))
						.getText();
				Reporter.log("Connected time on rate call screen for the first user -> " + rateReconnectedTime2, true);

				Reporter.log("First user clicking on the thumbs up.", true);
				waitindriver.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/iv_thumbs_up")))
						.click();
				
				Reporter.log("Clicking on the submit button." , true);
				waitindriver.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/card_submit")))
				.click();
				

				// Second user's rate call screen

			
				 Reporter.log("Facting connected username for the second user.", true);
				 String rateReconnectedUsername3 = waitindriver1
				 .until(ExpectedConditions.visibilityOfElementLocated(By.id(
				 "com.opentalk.staging:id/txt_user_name"))).getText(); 
				 Reporter.log(
				 "rate Reconnected Username1 -> " + rateReconnectedUsername3, true);
				 Assert.assertEquals(rateReconnectedUsername3, "Saurabh");
				  
				 Reporter.log("Fatching connected time for the second user.", true); String
				 rateReconnectedTime3 = waitindriver1.until(
				 ExpectedConditions.visibilityOfElementLocated(By.id(
				 "com.opentalk.staging:id/text_call_duration"))) .getText();
				 Reporter.log(
				 "Connected time on rate call screen for the second user -> " +
						 rateReconnectedTime3, true);
				 
				 Reporter.log("Second user clicking on the thumbs up.", true);
				 waitindriver1.until(ExpectedConditions.elementToBeClickable(By.id(
				 "com.opentalk.staging:id/iv_thumbs_up"))) .click();
				 
					Reporter.log("Clicking on the submit button." , true);
					waitindriver1.until(ExpectedConditions.elementToBeClickable(By.id("com.opentalk.staging:id/card_submit")))
					.click();*/
	}

	/*
	 * @Test(priority = 1) public void Verify_Users_Through_Mobile() throws
	 * MalformedURLException, InterruptedException {
	 * 
	 * waitindriver2 = new WebDriverWait(driver2,30); waitindriver = new
	 * WebDriverWait(driver,30);
	 * 
	 * driver2.get("https://app.mysms.com/#login");
	 * waitindriver2.until(ExpectedConditions.visibilityOfElementLocated(By.
	 * linkText("Sign in with your mobile number"))).click();
	 * Thread.sleep(1000); WebElement country_dropdwon =
	 * driver2.findElement(By.xpath(
	 * "html/body/div[4]/div[2]/div/div[2]/div/table/tbody/tr[3]/td/div[1]/table/tbody/tr[2]/td/div/table/tbody/tr[1]/td[1]/div/select"
	 * )); // WebElement country_dropdwon =
	 * waitindriver2.until(ExpectedConditions.visibilityOfElementLocated(By.
	 * xpath(
	 * "html/body/div[4]/div[2]/div/div[2]/div/table/tbody/tr[3]/td/div[1]/table/tbody/tr[2]/td/div/table/tbody/tr[1]/td[1]/div/select"
	 * ))); Select country = new Select(country_dropdwon);
	 * country.selectByValue("in");
	 * waitindriver2.until(ExpectedConditions.visibilityOfElementLocated(By.
	 * xpath(
	 * "html/body/div[4]/div[2]/div/div[2]/div/table/tbody/tr[3]/td/div[1]/table/tbody/tr[2]/td/div/table/tbody/tr[1]/td[2]/input"
	 * ))).sendKeys("7568031284");
	 * waitindriver2.until(ExpectedConditions.visibilityOfElementLocated(By.
	 * xpath(
	 * "html/body/div[4]/div[2]/div/div[2]/div/table/tbody/tr[3]/td/div[1]/table/tbody/tr[2]/td/div/table/tbody/tr[2]/td/div/input"
	 * ))).sendKeys("auriga123");
	 * waitindriver2.until(ExpectedConditions.elementToBeClickable(By.xpath(
	 * "html/body/div[4]/div[2]/div/div[2]/div/table/tbody/tr[3]/td/div[1]/table/tbody/tr[2]/td/div/table/tbody/tr[3]/td/button"
	 * ))).click(); Thread.sleep(10000);
	 * 
	 * 
	 * Reporter.log("First user clicking on the Login button.", true);
	 * waitindriver.until(ExpectedConditions.elementToBeClickable(By.id(
	 * "com.opentalk.staging:id/btn_phone_login"))).click();
	 * 
	 * Reporter.log("Clicking on coutntry code.", true);
	 * waitindriver.until(ExpectedConditions.elementToBeClickable(By.id(
	 * "com.opentalk:id/code_spinner"))).click();
	 * 
	 * Reporter.log("Selecting coutntry code.", true);
	 * waitindriver.until(ExpectedConditions.elementToBeClickable(By.id(
	 * "com.opentalk:id/code_spinner"))).click();
	 * 
	 * Reporter.log("Entering phone.", true);
	 * waitindriver.until(ExpectedConditions.elementToBeClickable(By.id(
	 * "com.opentalk.staging:id/edt_phone"))).sendKeys("7568031284");
	 * 
	 * Reporter.log("Clicking on Continue button.", true);
	 * waitindriver.until(ExpectedConditions.elementToBeClickable(By.id(
	 * "com.opentalk.staging:id/txt_done"))).click();
	 * 
	 * Thread.sleep(60000);
	 * driver2.get("https://app.mysms.com/#messages:51404"); List<WebElement>
	 * incoming = driver2.findElements(By.className("incoming"));
	 * List<WebElement> bubble =
	 * incoming.get(0).findElements(By.className("bubble")); List<WebElement>
	 * message = bubble.get(0).findElements(By.className("message"));
	 * Reporter.log("Getting Element", true); String e =
	 * message.get(0).getText(); Thread.sleep(1000); Reporter.log("Getting code"
	 * , true); Reporter.log(e, true); String code = e.split(" ")[0];
	 * Reporter.log(code, true);
	 * 
	 * Reporter.log("Entering data.", true);
	 * waitindriver.until(ExpectedConditions.elementToBeClickable(By.id(
	 * "com.opentalk.staging:id/edt_code_1"))).sendKeys(code.charAt(0)+"");
	 * Reporter.log("Entering data.", true);
	 * waitindriver.until(ExpectedConditions.elementToBeClickable(By.id(
	 * "com.opentalk.staging:id/edt_code_2"))).sendKeys(code.charAt(1)+"");
	 * Reporter.log("Entering data.", true);
	 * waitindriver.until(ExpectedConditions.elementToBeClickable(By.id(
	 * "com.opentalk.staging:id/edt_code_3"))).sendKeys(code.charAt(2)+"");
	 * Reporter.log("Entering data.", true);
	 * waitindriver.until(ExpectedConditions.elementToBeClickable(By.id(
	 * "com.opentalk.staging:id/edt_code_4"))).sendKeys(code.charAt(3)+"");
	 * Reporter.log("Entering data.", true);
	 * waitindriver.until(ExpectedConditions.elementToBeClickable(By.id(
	 * "com.opentalk.staging:id/edt_code_5"))).sendKeys(code.charAt(4)+"");
	 * Reporter.log("Entering data.", true);
	 * waitindriver.until(ExpectedConditions.elementToBeClickable(By.id(
	 * "com.opentalk.staging:id/edt_code_6"))).sendKeys(code.charAt(5)+"");
	 * 
	 * Reporter.log("Clicking on Continue button.", true);
	 * waitindriver.until(ExpectedConditions.elementToBeClickable(By.id(
	 * "com.opentalk.staging:id/txt_done"))).click();
	 * 
	 * 
	 * 
	 * //waitindriver2.until(ExpectedConditions.visibilityOfElementLocated(By.
	 * xpath(
	 * "html/body/div[4]/div[2]/div/div[2]/div/div[4]/div/div[2]/div/div[2]/div/div[4]/div/div/div/div[1]/div"
	 * ))).click();
	 * 
	 * 
	 * 
	 * }
	 */

	@BeforeClass
	public void beforeMethod() throws InterruptedException, IOException {

		Reporter.log("Verify the Login Functionality.", true);
		Reporter.log("Setting the Desired Capabilities.", true);

		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("deviceName", "Android GoogleAPI Emulator");
		capabilities.setCapability("platformVersion", "5.1");
		capabilities.setCapability("app", "sauce-storage:app-debug.apk.zip");
		capabilities.setCapability("browserName", "");
		capabilities.setCapability("deviceOrientation", "portrait");
		capabilities.setCapability("appiumVersion", "1.5.3");
		capabilities.setCapability("name", "OpenTalk Login Testing");
		capabilities.setCapability("appPackage", "com.opentalk.staging");
		capabilities.setCapability("idle-timeout", 500);
		capabilities.setCapability("autoAcceptAlerts", true);
		capabilities.setCapability("unicodeKeyboard", true);
		capabilities.setCapability("resetKeyboard", true);

		DesiredCapabilities capabilities1 = new DesiredCapabilities();
		capabilities1.setCapability("platformName", "Android");
		capabilities1.setCapability("deviceName", "Android GoogleAPI Emulator");
		capabilities1.setCapability("platformVersion", "5.1");
		capabilities1.setCapability("app", "sauce-storage:app-debug-new.apk.zip");
		capabilities1.setCapability("browserName", "");
		capabilities1.setCapability("deviceOrientation", "portrait");
		capabilities1.setCapability("appiumVersion", "1.5.3");
		capabilities1.setCapability("name", "OpenTalk Login Testing");
		capabilities1.setCapability("appPackage", "com.opentalk.staging");
		capabilities1.setCapability("idle-timeout", 500);
		capabilities1.setCapability("autoAcceptAlerts", true);
		capabilities1.setCapability("unicodeKeyboard", true);
		capabilities1.setCapability("resetKeyboard", true);

		/*
		 * DesiredCapabilities caps = DesiredCapabilities.firefox();
		 * caps.setCapability("platform", "Windows 8");
		 * caps.setCapability("version", "41.0");
		 */

		Reporter.log("Initializing First User's AndroidDriver.", true);
		driver = new AndroidDriver(new URL(URL), capabilities);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		Thread.sleep(3000);
		
/*		((AppiumDriver) driver).runAppInBackground(10);//put app in background for 10 seconds
		((AndroidDriver)driver).currentActivity();*/
		
		//((AppiumDriver) driver).closeApp();


/*		Reporter.log("Initializing Second User's AndroidDriver.", true);
		driver1 = new AndroidDriver(new URL(URL), capabilities1);
		driver1.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);*/

		/*
		 * Reporter.log("Opening Firefox browser.", true); driver2 = new
		 * RemoteWebDriver(new URL(URL), caps);
		 * driver2.manage().window().maximize();
		 */

	}

	@AfterClass

	public void teardown() {
		driver.quit();
		//driver1.quit();
		// driver2.quit();

	}

}
