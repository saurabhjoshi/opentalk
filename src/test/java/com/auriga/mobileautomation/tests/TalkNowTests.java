package com.auriga.mobileautomation.tests;

import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class TalkNowTests extends TestBase {
	
/*	@BeforeClass
	public void signIn() throws InterruptedException
	{
		OpenTalkUsers.getInstance().getPrimaryUser().doFBSignIn(Constant.firstUserEmail, Constant.firstUserPassword, Constant.firstUserLanguage);
		OpenTalkUsers.getInstance().getSecondryUser().doFBSignIn(Constant.secondUserEmail, Constant.secondUserPassword, Constant.secondUserLanguage);

	}*/

	
	@Test(priority = 0)
	public void talkNowDisabled()
	{
		Reporter.log("Verifying that default talk now button is disabled for both the users." , true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().isTalkNowDisabled());
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().isTalkNowDisabled());
		Reporter.log("Buttons are disabled." , true);
		
	}
	
/*	@Test (priority = 1)
	public void talkNowEnabled()
	{
		Reporter.log("Verifying that after selecting any topic the talk now button is enabled." , true);
		OpenTalkUsers.getInstance().getPrimaryUser().selectTopic(Constant.firstUserTopicIndex);
	//	Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().isTalkNowEnabled());
		OpenTalkUsers.getInstance().getSecondryUser().selectTopic(Constant.secondUserTopicIndex);
	//	Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().isTalkNowEnabled());
		Reporter.log("Buttons are enabled." , true);
	}
	
	@Test (priority = 2,dependsOnMethods = {"talkNowEnabled"})
	public void isFindingPartnerScreen()
	{
		Reporter.log("Verifying that the user is redirected to the finding partner screen after clicking on the talk now button." , true);
		OpenTalkUsers.getInstance().getPrimaryUser().TalkNow();
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().isFindingPartnerScreen());
		OpenTalkUsers.getInstance().getSecondryUser().TalkNow();
//		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().isFindingPartnerScreen());
		Reporter.log("Both users are on finding partner screen." , true);
	}
	
	@Test(priority = 3,dependsOnMethods = {"isFindingPartnerScreen"})
	public void isConnectingScreen()
	{
		Reporter.log("Verifying that the connecting screen is visible after the finding partner screen when call connects." , true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().isConnecting());
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().isConnecting());
		Reporter.log("Both users are connecting." , true);
		
	}
	
	@Test(priority = 4,dependsOnMethods = {"isConnectingScreen"})
	public void verifyUserInfoOnConnectingScreen()
	{
		Reporter.log("Verifying the connecting user info." , true);
		Map<String , String> m =  OpenTalkUsers.getInstance().getPrimaryUser().getUserInfoOnConnectingScreen();
		String userName = m.get("userName");
	    Reporter.log("Connecting username - > " + userName, true);
	    Assert.assertTrue(userName.contains(Constant.secondUserName));
	    
	    String lan = m.get("languages");
	    Reporter.log("Connecting user language  - > " + lan, true);
	    Assert.assertTrue(lan.contains(Constant.secondUserLanguage));
	    
	    String topic = m.get("topic");
	    Reporter.log("Connecting user topic  - > " + topic, true);
	    Assert.assertTrue(topic.contains(OpenTalkUsers.getInstance().getPrimaryUser().topic));
	    
	}
	
	@Test(priority = 5,dependsOnMethods = {"isConnectingScreen"})
	public void isConnectedScreen()
	{
		Reporter.log("Verifying that the call connected screen is visible after the call connecting screen when call connects." , true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().isConnected());
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().isConnected());
		Reporter.log("Both users are connected." , true);
		
	}
	
	@Test(priority = 6,dependsOnMethods = {"isConnectedScreen"})
	public void verifyInfoOnConnectedScreen()
	{
		Reporter.log("Verifying the connected user info." , true);
		Map<String , String> m =  OpenTalkUsers.getInstance().getPrimaryUser().getInfoOnConnectedScreen();
		String userName = m.get("userName");
	    Reporter.log("Connected username - > " + userName, true);
	    Assert.assertTrue(userName.contains(Constant.secondUserName));
	    
	    
	    String status = m.get("status");
	    Reporter.log("status  - > " + status, true);
	    Assert.assertTrue(status.contains("Connected"));
	    
	}
	
	@Test(priority = 7,dependsOnMethods = {"isConnectedScreen"})
	public void isExtendCallEnabled()
	{
		Reporter.log("Verifying that extend call button is enabled.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().isExtendCallEnabled());
	}
	
	@Test(priority = 8,dependsOnMethods = {"isConnectedScreen"})
	public void isCallExtendForFirstTime() throws InterruptedException
	{
		Reporter.log("Verifying that call extends when auser clicks on the extend call button .", true);
		SoftAssert soft = new SoftAssert();
		soft.assertEquals("a", "b");
		soft.assertEquals(1, 2);
		soft.assertAll();
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().isCallExtended());
	}
	
	@Test(priority = 9,dependsOnMethods = {"isCallExtendForFirstTime"})
	public void isCallExtendForSecondTime() throws InterruptedException
	{
		Reporter.log("Verifying that call extends when auser clicks on the extend call button .", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().isCallExtended());
	}
	
	@Test(priority = 10,dependsOnMethods = {"isCallExtendForFirstTime"})
	public void UserOnRateCall() throws InterruptedException{
		
		Reporter.log("Verifying that the user on rate call screen after clicks  on stop connecting.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().clicksOnEndCall().isRateCallScreen());
		Reporter.log("Verifying that second user is also on Rate screen.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().isRateCallScreen());
	}
	
	@Test(priority = 11,dependsOnMethods = {"UserOnRateCall"})
	public void verifyInfoOnRateScreen()
	{
		Reporter.log("Verifying the connected user info on rate call screen." , true);
		Map<String , String> m =  OpenTalkUsers.getInstance().getPrimaryUser().getInfoOnRateScreen();
		String userName = m.get("userName");
	    Reporter.log("Connected username on rate call screen - > " + userName, true);
	    Assert.assertTrue(userName.contains(Constant.secondUserName));
	    
		Reporter.log("Verifying the second connected user info on rate call screen." , true);
		Map<String , String> m1 =  OpenTalkUsers.getInstance().getSecondryUser().getInfoOnRateScreen();
		String userName1 = m1.get("userName");
	    Reporter.log("Connected username on rate call screen - > " + userName1, true);
	    Assert.assertTrue(userName1.contains(Constant.firstUserName));
	    
	}
	
	@Test(priority = 12,dependsOnMethods = {"verifyInfoOnRateScreen"})
	public void isComplementScreen(){
		
		Reporter.log("Verifying that the user on complement screen after clicks  on thumbs up.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().cliksOnThumbsUp().isComplementScreen());
		Reporter.log("Verifying that the second user on complement screen after clicks  on thumbs up.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().cliksOnThumbsUp().isComplementScreen());
	}
	
	@Test(priority = 13,dependsOnMethods = {"isComplementScreen"})
	public void isFindingPartnerScreenAfterThumbsupForFirstUser(){
		
		Reporter.log("Verifying that the user on finding partner screen.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().clicksOnSubmitOnComplement().isFindingPartnerScreen());

	}
	
	@Test(priority = 14,dependsOnMethods = {"isFindingPartnerScreenAfterThumbsupForFirstUser"})
	public void isHomeAfterStopeConnectingForFirstUser(){
		
		Reporter.log("Verifying that the user on Home screen.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().clicksOnStopConnecting().isHomeScreen());

	}
	
	@Test(priority = 15,dependsOnMethods = {"isHomeAfterStopeConnectingForFirstUser"})
	public void isFindingPartnerScreenAfterThumbsupForSecondUser(){
		
		Reporter.log("Verifying that the second user on finding partner screen.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().clicksOnSubmitOnComplement().isFindingPartnerScreen());
	}
	
	@Test(priority = 16,dependsOnMethods = {"isFindingPartnerScreenAfterThumbsupForSecondUser"})
	public void isHomeAfterStopeConnectingForSecondUser(){
		
		Reporter.log("Verifying that the user on Home screen.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().clicksOnStopConnecting().isHomeScreen());

	}*/

}

