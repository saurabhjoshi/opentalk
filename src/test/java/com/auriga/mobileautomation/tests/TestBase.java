package com.auriga.mobileautomation.tests;

import java.net.MalformedURLException;

import org.json.JSONException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

public class TestBase {
	
	@BeforeTest
	public void OpenApp() throws MalformedURLException, InterruptedException
	{
		OpenTalkUsers.getInstance().setPrimaryUser(new OpenTalkApp(Constant.firstUserApp,Constant.firstUserAndroidVersion));
		OpenTalkUsers.getInstance().setSecondryUser(new OpenTalkApp(Constant.secondUserApp,Constant.secondUserAndroidVersion));
	
		OpenTalkUsers.getInstance().getPrimaryUser().doFBSignIn(Constant.firstUserEmail, Constant.firstUserPassword, Constant.firstUserLanguage);
		OpenTalkUsers.getInstance().getSecondryUser().doFBSignIn(Constant.secondUserEmail, Constant.secondUserPassword, Constant.secondUserLanguage);
	}
	
	
/*	@BeforeMethod
	public void beforeTestAppLogs() throws JSONException
	{
		OpenTalkUsers.getInstance().getPrimaryUser().gettingAppLogs();
	//	OpenTalkUsers.getInstance().getSecondryUser().gettingAppLogs();
	}*/
	
	@AfterMethod
	public void afterTestAppLogs() throws JSONException
	{
		OpenTalkUsers.getInstance().getPrimaryUser().gettingErrorLogs();
		OpenTalkUsers.getInstance().getSecondryUser().gettingErrorLogs();
	}
	
	@AfterTest
	public void closeApp()
	{
		OpenTalkUsers.getInstance().getPrimaryUser().quit();
		OpenTalkUsers.getInstance().getSecondryUser().quit();
	}

}
