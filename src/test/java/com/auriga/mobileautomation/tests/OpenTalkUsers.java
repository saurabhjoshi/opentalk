package com.auriga.mobileautomation.tests;

public class OpenTalkUsers {
	private static final OpenTalkUsers instance = new OpenTalkUsers();
	
	private OpenTalkUsers(){}
	
	private OpenTalkApp primaryUser;
	private OpenTalkApp secondryUser;
	
	public static OpenTalkUsers getInstance(){
		return instance;
	}
	
	public void setPrimaryUser(OpenTalkApp primaryUser){
		this.primaryUser = primaryUser;
	}
	
	public OpenTalkApp getPrimaryUser(){
		return this.primaryUser;
	}
	
	public void setSecondryUser(OpenTalkApp secondryUser){
		this.secondryUser = secondryUser;
	}
	
	public OpenTalkApp getSecondryUser(){
		return this.secondryUser;
	}

}
