package com.auriga.mobileautomation.tests;

public class Constant {
	
	public static final String USERNAME = "ChandanOpentalk";
	public static final String ACCESS_KEY = "f7d687a4-93ea-42c1-8f27-36064b561f73";
	public static final String URL = "http://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:80/wd/hub";
	
	public static final String firstUserApp = "sauce-storage:app-debug.apk.zip";
	public static final String firstUserAndroidVersion = "5.1";
	public static final String firstUserName = "Saurabh";
	public static final String firstUserEmail = "saurabh.joshi@aurigait.com";
	public static final String firstUserPassword = "testing123";
	public static final String firstUserLanguage = "Bali";
	public static final int firstUserTopicIndex = 0;
	public static final String firstUserTalkRequestNote = "Test Note";
	public static final String firstUserTalkStatus = "Test Status";
	public static String firstUserTopic ;
	
	public static final String secondUserApp = "sauce-storage:app-debug.apk.zip";
	public static final String secondUserAndroidVersion = "5.1";
	public static final String secondUserName = "Purusottam";
	public static final String secondUserEmail = "sonupk2007@gmail.com";
	public static final String secondUserPassword = "sonupk2007(sonu)";
	public static final String secondUserLanguage = "Bali";
	public static final int secondUserTopicIndex = 0;
	public static String secondUserTopic;

}
