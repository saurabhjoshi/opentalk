package com.auriga.mobileautomation.tests;

import java.net.MalformedURLException;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.android.AndroidDriver;

public class OpenTalkTest extends TestBase {
	

	@Test(groups = {"SingIn"})
	public void firstUserOnBorading() throws MalformedURLException, InterruptedException
	{
		OpenTalkUsers.getInstance().getPrimaryUser().doFBSignIn(Constant.firstUserEmail, Constant.firstUserPassword, Constant.firstUserLanguage);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().verifyUsername(Constant.firstUserName));
	}
	
/*	@Test(groups = {"SingIn"})
	public void secondUserOnBorading() throws MalformedURLException, InterruptedException
	{
		OpenTalkUsers.getInstance().getSecondryUser().doFBSignIn(Constant.secondUserEmail, Constant.secondUserPassword, Constant.secondUserLanguage);
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().verifyUsername(Constant.secondUserName));
	}*/
	
	@Test ()
	public void verifyingConnectingScreenForFirstUser()
	{
		
	}
	
/*	@BeforeClass
	public void OpenApp() throws MalformedURLException
	{
		OpenTalkUsers.getInstance().setPrimaryUser(new OpenTalkApp(Constant.firstUserApp,Constant.firstUserAndroidVersion));
	//	OpenTalkUsers.getInstance().setSecondryUser(new OpenTalkApp(Constant.secondUserApp,Constant.secondUserAndroidVersion));
	}
	
	@AfterClass
	public void closeApp()
	{
		OpenTalkUsers.getInstance().getPrimaryUser().quit();
		//OpenTalkUsers.getInstance().getSecondryUser().quit();
	}*/

}
