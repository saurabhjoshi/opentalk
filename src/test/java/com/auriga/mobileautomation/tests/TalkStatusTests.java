package com.auriga.mobileautomation.tests;

import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TalkStatusTests extends TestBase{
	
/*	@BeforeClass
	public void signIn() throws InterruptedException
	{
		OpenTalkUsers.getInstance().getPrimaryUser().doFBSignIn(Constant.firstUserEmail, Constant.firstUserPassword, Constant.firstUserLanguage);
		OpenTalkUsers.getInstance().getSecondryUser().doFBSignIn(Constant.secondUserEmail, Constant.secondUserPassword, Constant.secondUserLanguage);

	}*/
	
	@Test(priority = 0)
	public void isMyTalkCircle()
	{
		Reporter.log("Verifying that the user is on my talk circle.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().clicksOnMyTalkCircle().isUserOnMyTalkCircle());
	}

	@Test(priority = 1)
	public void isCreateTalkStatusScreen()
	{
		Reporter.log("Verifying that the user is on create talk status screen after clicking on talk status bar.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().clicksOnMyTalkStatusBar().isCreatTalkStatusScreen());
	}
	
	@Test(priority = 2)
	public void verifyTheUserTalkStatus()
	{
		Reporter.log("Verifying the talk status created by the user in status bar.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().creatTalkStatus().getTalkStatus().contains(Constant.firstUserTalkStatus));
	}
	
	@Test(priority = 3)
	public void isSentReceivedScreen()
	{
		Reporter.log("Verifying that the user is on sent received screen after creating a talk request icon.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().clicksOnSentReceivedIcon().isSentReceivedRequestScreen());
	}
	
	@Test(priority = 4)
	public void verifyingTalkStatusInfoInSentRequests()
	{
		Reporter.log("Verifying talk status info in sent requests." , true);
		Map<String , String> m =  OpenTalkUsers.getInstance().getPrimaryUser().clicksOnSentTab().getTalkStatusInfoOnSent();
		String talkstatus = m.get("talkStatus");
	    Reporter.log("talkstatus visible in sent request - > " + talkstatus, true);
	    Assert.assertTrue(talkstatus.contains(Constant.firstUserTalkStatus));
	    
	    String lan = m.get("circleName");
	    Reporter.log("circleName is  - > " + lan, true);
	    Assert.assertTrue(lan.contains("My Talk Circle"));
	    
		
	}
	
	@Test(priority = 5)
	public void isMyTalkCircleForSecondUser()
	{
		Reporter.log("Verifying that the user is on my talk circle.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().clicksOnMyTalkCircle().isUserOnMyTalkCircle());
	}
	
	@Test(priority = 6)
	public void verifyingTalkStatusInfoMemberList()
	{
		Reporter.log("Verifying talk status info in group member list." , true);
		Map<String , String> m =  OpenTalkUsers.getInstance().getSecondryUser().getTalkStatusInfoOnCircleMemberList();
		String userName = m.get("userName");
	    Reporter.log("username visible in sent request - > " + userName, true);
	    Assert.assertTrue(userName.contains(Constant.firstUserName));
		
		String talkstatus = m.get("talkStatus");
	    Reporter.log("talkstatus visible in sent request - > " + talkstatus, true);
	    Assert.assertTrue(talkstatus.contains(Constant.firstUserTalkStatus));
	    
	    String lan = m.get("circleName");
	    Reporter.log("circleName is  - > " + lan, true);
	    Assert.assertTrue(lan.contains("My Talk Circle"));
		
	}
	
	@Test(priority = 7)
	public void isFindingPartnerScreen()
	{
		Reporter.log("Verifying that the user is on finding partner.", true);
		OpenTalkUsers.getInstance().getSecondryUser().clickOnStatusCallIcon();
	}
	
	@Test(priority = 8)
	public void verifyUserInfoOnConnectingScreen()
	{
		Reporter.log("Verifying the connecting user info." , true);
		Map<String , String> m =  OpenTalkUsers.getInstance().getPrimaryUser().getUserInfoOnConnectingScreen();
		String userName = m.get("userName");
	    Reporter.log("Connecting username - > " + userName, true);
	    Assert.assertTrue(userName.contains(Constant.secondUserName));
	    
	    String lan = m.get("languages");
	    Reporter.log("Connecting user language  - > " + lan, true);
	    Assert.assertTrue(lan.contains(Constant.secondUserLanguage));
	    
	    String topic = m.get("topic");
	    Reporter.log("Connecting user topic  - > " + topic, true);
	    Assert.assertTrue(topic.contains(Constant.firstUserTalkStatus));
	    
	}
	
	@Test(priority = 9)
	public void isConnectedScreen()
	{
		Reporter.log("Verifying that the call connected screen is visible after the call connecting screen when call connects." , true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().isConnected());
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().isConnected());
		Reporter.log("Both users are connected." , true);
		
	}
	
	@Test(priority = 10)
	public void verifyInfoOnConnectedScreen()
	{
		Reporter.log("Verifying the connected user info." , true);
		Map<String , String> m =  OpenTalkUsers.getInstance().getPrimaryUser().getInfoOnConnectedScreen();
		String userName = m.get("userName");
	    Reporter.log("Connected username - > " + userName, true);
	    Assert.assertTrue(userName.contains(Constant.secondUserName));
	    
	    
	    String status = m.get("status");
	    Reporter.log("status  - > " + status, true);
	    Assert.assertTrue(status.contains("Connected"));
	    
	}
	
	@Test(priority = 11)
	public void isExtendCallEnabled()
	{
		Reporter.log("Verifying that extend call button is enabled.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().isExtendCallEnabled());
	}
	
	@Test(priority = 12)
	public void isCallExtendForFirstTime() throws InterruptedException
	{
		Reporter.log("Verifying that call extends when auser clicks on the extend call button .", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().isCallExtended());
	}
	
	@Test(priority = 13)
	public void isCallExtendForSecondTime() throws InterruptedException
	{
		Reporter.log("Verifying that call extends when auser clicks on the extend call button .", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().isCallExtended());
	}
	
	@Test(priority = 14)
	public void UserOnRateCall() throws InterruptedException{
		
		Reporter.log("Verifying that the user on rate call screen after clicks  on stop connecting.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().clicksOnEndCall().isRateCallScreen());
		Reporter.log("Verifying that second user is also on Rate screen.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().isRateCallScreen());
	}
	
	@Test(priority = 15)
	public void verifyInfoOnRateScreen()
	{
		Reporter.log("Verifying the connected user info on rate call screen." , true);
		Map<String , String> m =  OpenTalkUsers.getInstance().getPrimaryUser().getInfoOnRateScreen();
		String userName = m.get("userName");
	    Reporter.log("Connected username on rate call screen - > " + userName, true);
	    Assert.assertTrue(userName.contains(Constant.secondUserName));
	    
		Reporter.log("Verifying the second connected user info on rate call screen." , true);
		Map<String , String> m1 =  OpenTalkUsers.getInstance().getSecondryUser().getInfoOnRateScreen();
		String userName1 = m1.get("userName");
	    Reporter.log("Connected username on rate call screen - > " + userName1, true);
	    Assert.assertTrue(userName1.contains(Constant.firstUserName));
	    
	}
	
	@Test(priority = 16)
	public void isComplementScreen(){
		
		Reporter.log("Verifying that the user on complement screen after clicks  on thumbs up.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().cliksOnThumbsUp().isComplementScreen());
		Reporter.log("Verifying that the second user on complement screen after clicks  on thumbs up.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().cliksOnThumbsUp().isComplementScreen());
	}
	
	@Test(priority = 17)
	public void isSentReceviedScreenAfterThumbsupForFirstUser(){
		
		Reporter.log("Verifying that the user on sent recevied screen after submit.", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getPrimaryUser().clicksOnSubmitOnComplement().isSentReceivedRequestScreen());

	}
	
	
	@Test(priority = 18)
	public void isSentReceviedScreenAfterThumbsupForSecondUser(){
		
		Reporter.log("Verifying that the user on sent recevied screen after submit..", true);
		Assert.assertTrue(OpenTalkUsers.getInstance().getSecondryUser().clicksOnSubmitOnComplement().isSentReceivedRequestScreen());
		
	}
}
