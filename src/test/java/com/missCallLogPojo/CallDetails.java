
package com.missCallLogPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CallDetails {

    @SerializedName("version")
    @Expose
    private Long version;
    @SerializedName("call_id")
    @Expose
    private Long callId;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("created_at")
    @Expose
    private Long createdAt;
    @SerializedName("request_user_id")
    @Expose
    private Long requestUserId;
    @SerializedName("receiver_user_id")
    @Expose
    private Long receiverUserId;
    @SerializedName("topic_id")
    @Expose
    private Long topicId;
    @SerializedName("requester_topic_id")
    @Expose
    private Long requesterTopicId;
    @SerializedName("receiver_topic_id")
    @Expose
    private Long receiverTopicId;
    @SerializedName("call_type")
    @Expose
    private String callType;
    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("unique_id")
    @Expose
    private String uniqueId;
    @SerializedName("opentok_call_id")
    @Expose
    private String opentokCallId;
    @SerializedName("reason_status")
    @Expose
    private Long reasonStatus;

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Long getCallId() {
        return callId;
    }

    public void setCallId(Long callId) {
        this.callId = callId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getRequestUserId() {
        return requestUserId;
    }

    public void setRequestUserId(Long requestUserId) {
        this.requestUserId = requestUserId;
    }

    public Long getReceiverUserId() {
        return receiverUserId;
    }

    public void setReceiverUserId(Long receiverUserId) {
        this.receiverUserId = receiverUserId;
    }

    public Long getTopicId() {
        return topicId;
    }

    public void setTopicId(Long topicId) {
        this.topicId = topicId;
    }

    public Long getRequesterTopicId() {
        return requesterTopicId;
    }

    public void setRequesterTopicId(Long requesterTopicId) {
        this.requesterTopicId = requesterTopicId;
    }

    public Long getReceiverTopicId() {
        return receiverTopicId;
    }

    public void setReceiverTopicId(Long receiverTopicId) {
        this.receiverTopicId = receiverTopicId;
    }

    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getOpentokCallId() {
        return opentokCallId;
    }

    public void setOpentokCallId(String opentokCallId) {
        this.opentokCallId = opentokCallId;
    }

    public Long getReasonStatus() {
        return reasonStatus;
    }

    public void setReasonStatus(Long reasonStatus) {
        this.reasonStatus = reasonStatus;
    }

}
